use Test2::V0;
use Test2::Tools::Spec;

use Popsicle::Slope;

describe 'Overloaded methods' => sub {
    tests 'Stringification' => sub {
        my $slope = Popsicle::Slope->new( 2, 3 );
        is "$slope", '1.5';
    };

    tests 'Numification' => sub {
        my $slope = Popsicle::Slope->new( 2, 3 );
        is 0+$slope, 1.5;
    };

    tests 'Numeric comparison' => sub {
        my $base  = Popsicle::Slope->new( 5, 5 );
        my $long  = Popsicle::Slope->new( 1, 2 );
        my $short = Popsicle::Slope->new( 2, 1 );

        is $base <=> $long,  -1, '<=> == -1';
        is $base <=> $short,  1, '<=> ==  1';
        is $base <=> $base,   0, '<=> ==  0';

        is $base <=> 2, -1, '<=> == -1 with scalar';
        is $base <=> 0,  1, '<=> ==  1 with scalar';
        is $base <=> 1,  0, '<=> ==  0 with scalar';

        is $base < $long,  T, 'True  <';
        is $base < $short, F, 'False <';

        is $base < 2, T, 'True  < with scalar';
        is $base < 0, F, 'False < with scalar';

        is $base > $long,  F, 'False >';
        is $base > $short, T, 'True  >';

        is $base > 2, F, 'False > with scalar';
        is $base > 0, T, 'True  > with scalar';
    };

    tests 'Addition' => sub {
        my $slope1 = Popsicle::Slope->new( 1, 2 );
        my $slope2 = Popsicle::Slope->new( 3, 6 );

        is $slope1 + $slope2, 4, 'Slope + Slope';

        is $slope1 + 2, 4, 'Slope + Scalar';

        is $slope1 + 0, 2, 'Slope + 0';

        is 2 + $slope1, 4, 'Scalar + Slope';

        like dies { $slope1 + undef },
            qr/Missing parameter in slope arithmetic/,
            'Slope + Undef dies';
    };

    tests 'Subtraction' => sub {
        my $slope1 = Popsicle::Slope->new( 1, 2 );
        my $slope2 = Popsicle::Slope->new( 3, 6 );

        is $slope1 - $slope2, 0, 'Slope - Slope';

        is $slope1 - 2, 0, 'Slope - Scalar';

        is $slope1 - 0, 2, 'Slope - 0';

        is 0 - $slope1, -2, '0 - Slope';

        like dies { $slope1 - undef },
            qr/Missing parameter in slope arithmetic/,
            'Slope - Undef dies';
    };

    tests 'Division' => sub {
        my $slope1 = Popsicle::Slope->new( 1, 3 );
        my $slope2 = Popsicle::Slope->new( 5, 5 );

        is $slope1 / $slope2, 3, 'Slope / Slope';

        is $slope1 / 2, 1.5, 'Slope / Scalar';

        is $slope1 / 1, 3, 'Slope / 1';

        is 6 / $slope1, 2, 'Scalar / Slope';

        like dies { $slope1 / undef },
            qr/Missing parameter in slope arithmetic/,
            'Slope / Undef dies';

        like dies { $slope1 / 0 },
            qr/illegal division by zero/i,
            'Slope / 0 dies';
    };

    tests 'Multiplication' => sub {
        my $slope1 = Popsicle::Slope->new( 1, 2 );
        my $slope2 = Popsicle::Slope->new( 4, 12 );

        is $slope1 * $slope2, 6, 'Slope * Slope';

        is $slope1 * 2, 4, 'Slope * Scalar';

        is 2 * $slope1, 4, 'Scalar * Slope';

        is $slope1 * 1, 2, 'Slope * 1';

        is $slope1 * 0, 0, 'Slope * 0';

        like dies { $slope1 * undef },
            qr/Missing parameter in slope arithmetic/,
            'Slope * Undef dies';
    };

    tests 'Unary minus' => sub {
        my $slope1 = Popsicle::Slope->new( 5, 5 );
        is -$slope1, -1;
    };
};

describe 'Construction' => sub {
    tests 'From arrayref' => sub {
        is +Popsicle::Slope->new( [ 1, 2 ] ), object {
            prop blessed => 'Popsicle::Slope';
            call x => 1;
            call y => 2;
        };
    };

    tests 'From array' => sub {
        is +Popsicle::Slope->new( 2, 3 ), object {
            prop blessed => 'Popsicle::Slope';
            call x => 2;
            call y => 3;
        };
    };

    tests 'From slope' => sub {
        my $slope = Popsicle::Slope->new( 2, 3 );

        is +Popsicle::Slope->new( $slope ), object {
            prop blessed => 'Popsicle::Slope';
            prop this => not_in_set( exact_ref $slope );
            call x => 2;
            call y => 3;
        };
    };

    tests 'As method call' => sub {
        my $slope = Popsicle::Slope->new( 1, 2 );

        is $slope->new( 2, 3 ), object {
            prop blessed => 'Popsicle::Slope';
            prop this => not_in_set( exact_ref $slope );
            call x => 2;
            call y => 3;
        };

        is $slope, object {
            call x => 1;
            call y => 2;
        };
    };
};

describe 'Object modification' => sub {
    tests 'Lvalue accesors' => sub {
        my $slope = Popsicle::Slope->new( 1, 2 );

        is $slope->x, 1, 'Read x';
        is $slope->y, 2, 'Read y';

        $slope->x = 10;
        $slope->y = 11;

        is $slope->x, 10, 'Set x';
        is $slope->y, 11, 'Set y';
    };

    tests 'Direct access' => sub {
        my $slope = Popsicle::Slope->new( 1, 2  );

        is $slope->[0], 1, 'Read raw x';
        is $slope->[1], 2, 'Read raw y';

        is $slope, [ 1, 2 ], 'Check initial components';

        $slope->[0] = 10;
        $slope->[1] = 11;

        is $slope->[0], 10, 'Set raw x';
        is $slope->[1], 11, 'Set raw y';

        is $slope, [ 10, 11 ], 'Check end components';
    };
};

done_testing;
