use Test2::V0;
use Test2::Tools::Spec;

use Popsicle::UI::HBox;

use Log::Any::Adapter 'Stderr';

tests 'Basic' => sub {
    my $window = Popsicle::UI::HBox->new(
        width  => 20,
        height => 15,
    );

    is $window, object {
        call dimensions => object { call_list components => [ 20, 15 ] };
        call position   => object { call_list components => [  0,  0 ] };
        call orient     => 'horizontal';
        call position_is_relative => F;

        call_list children => [];
    }, 'Sets attributes on construction';

    my $a = Popsicle::UI::HBox->new( width => 2, height => 1 );
    my $b = Popsicle::UI::HBox->new( width => 3, height => 3 );
    my $c = Popsicle::UI::HBox->new( width => 2, height => 2 );

    is $window->add_child($a), exact_ref $a, 'Adding child returns child';

    like dies { $window->add_child($a) },
        qr/Cannot add same element twice/,
        'Cannot add same element twice';

    $window->add_child($b);
    $window->add_child($c);

    is $window, object {
        call_list children => array {
            item object {
                prop this => exact_ref $a;
                call position => object { call_list components => [ 0, 0 ] };
            };
            item object {
                prop this => exact_ref $b;
                call position => object { call_list components => [ 2, 0 ] };
            };
            item object {
                prop this => exact_ref $c;
                call position => object { call_list components => [ 5, 0 ] };
            };
            end;
        };
    }, 'Children have position set after adding';

    is $window->remove_child($b), exact_ref $b, 'Remove child returns child';
    is $window->remove_child($b), exact_ref $b,
        'Remove child returns child even if not a child';

    is $window, object {
        call_list children => array {
            item object {
                prop this => exact_ref $a;
                call position => object { call_list components => [ 0, 0 ] };
            };
            item object {
                prop this => exact_ref $c;
                call position => object { call_list components => [ 2, 0 ] };
            };
            end;
        };
    }, 'Children have position set after removal';
};

tests 'Flex' => sub {
    my $window = Popsicle::UI::HBox->new(
        position => [ 1, 2 ],
        width  => 28,
        height => 12,
    );

    my $a = Popsicle::UI::HBox->new( width => 1, height => 4, flex => 0 );
    my $b = Popsicle::UI::HBox->new( width => 2, height => 3, flex => 1 );
    my $c = Popsicle::UI::HBox->new( width => 3, height => 2, flex => 2 );
    my $d = Popsicle::UI::HBox->new( width => 4, height => 1, flex => 3 );

    $window->add_child($a);
    $window->add_child($b);
    $window->add_child($c);
    $window->add_child($d);

    is $window, object {
        call_list children => array {
            item object {
                prop this => exact_ref $a;
                call position   => object { call_list components => [ 1, 2 ] };
                call dimensions => object { call_list components => [ 1, 12 ] };
            };
            item object {
                prop this => exact_ref $b;
                call position   => object { call_list components => [ 2, 2 ] };
                call dimensions => object { call_list components => [ 5, 12 ] };
            };
            item object {
                prop this => exact_ref $c;
                call position   => object { call_list components => [ 7, 2 ] };
                call dimensions => object { call_list components => [ 9, 12 ] };
            };
            item object {
                prop this => exact_ref $d;
                call position   => object { call_list components => [ 16, 2 ] };
                call dimensions => object { call_list components => [ 13, 12 ] };
            };
            end;
        };
    }, 'Extra space distributed according to flex values';
};

done_testing;
