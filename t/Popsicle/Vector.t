use Test2::V0;
use Test2::Tools::Spec;

use Popsicle::Vector;

describe 'Overloaded methods' => sub {
    tests 'Stringification' => sub {
        my $vector = Popsicle::Vector->new( 1, 2, 3 );
        is "$vector", '1,2,3';
    };

    tests 'String comparison' => sub {
        my $base  = Popsicle::Vector->new( 1, 2, 3 );
        my $after = Popsicle::Vector->new( 3, 4, 5 );

        is $base  cmp $after, -1, 'cmp == -1';
        is $after cmp $base,   1, 'cmp ==  1';
        is $base  cmp $base,   0, 'cmp ==  0';

        is $base  lt $after, T, 'True  lt';
        is $after lt $base,  F, 'False lt';

        is $base  gt $after, F, 'False gt';
        is $after gt $base,  T, 'True  gt';
    };

    tests 'Numeric comparison' => sub {
        my $base  = Popsicle::Vector->new( 1,   0 );
        my $long  = Popsicle::Vector->new( 0.9, 0.9 );
        my $short = Popsicle::Vector->new( 0.5, 0.5 );

        is $base <=> $long,  -1, '<=> == -1';
        is $base <=> $short,  1, '<=> ==  1';
        is $base <=> $base,   0, '<=> ==  0';

        is $base <=> 2, -1, '<=> == -1 with scalar';
        is $base <=> 0,  1, '<=> ==  1 with scalar';
        is $base <=> 1,  0, '<=> ==  0 with scalar';

        is $base < $long,  T, 'True  <';
        is $base < $short, F, 'False <';

        is $base < 2, T, 'True  < with scalar';
        is $base < 0, F, 'False < with scalar';

        is $base > $long,  F, 'False >';
        is $base > $short, T, 'True  >';

        is $base > 2, F, 'False > with scalar';
        is $base > 0, T, 'True  > with scalar';
    };

    tests 'Addition' => sub {
        my $vector1 = Popsicle::Vector->new( 1, 2, 3 );
        my $vector2 = Popsicle::Vector->new( 3, 4, 5 );

        is $vector1 + $vector2, object {
            prop blessed => 'Popsicle::Vector';
            call_list components => [ 4, 6, 8 ];
            call      dimensions => 3;
        }, 'Vector + Vector';

        is $vector1 + 2, object {
            prop blessed => 'Popsicle::Vector';
            call_list components => [ 3, 4, 5 ];
            call      dimensions => 3;
        }, 'Vector + Scalar';

        is $vector1 + 0, object {
            prop blessed => 'Popsicle::Vector';
            call_list components => [ 1, 2, 3 ];
            call      dimensions => 3;
        }, 'Vector + 0';

        is $vector1 + [ 6, 7, 8 ], object {
            prop blessed => 'Popsicle::Vector';
            call_list components => [ 7, 9, 11 ];
            call      dimensions => 3;
        }, 'Vector + ArrayRef';

        like dies { $vector1 + undef },
            qr/Missing parameter in vector arithmetic/,
            'Vector + Undef dies';

        like dies { 2 + $vector1 },
            qr/operation not allowed/,
            'Scalar + Vector dies';
    };

    tests 'Subtraction' => sub {
        my $vector1 = Popsicle::Vector->new( 1, 2 );
        my $vector2 = Popsicle::Vector->new( 3, 4 );

        is $vector1 - $vector2, object {
            prop blessed => 'Popsicle::Vector';
            call_list components => [ -2, -2 ];
            call      dimensions => 2;
        }, 'Vector - Vector';

        is $vector1 - 2, object {
            prop blessed => 'Popsicle::Vector';
            call_list components => [ -1, 0 ];
            call      dimensions => 2;
        }, 'Vector - Scalar';

        is $vector1 - 0, object {
            prop blessed => 'Popsicle::Vector';
            call_list components => [ 1, 2 ];
            call      dimensions => 2;
        }, 'Vector - 0';

        is $vector1 - [ 3, 4 ], object {
            prop blessed => 'Popsicle::Vector';
            call_list components => [ -2, -2 ];
            call      dimensions => 2;
        }, 'Vector - ArrayRef';

        like dies { $vector1 - undef },
            qr/Missing parameter in vector arithmetic/,
            'Vector - Undef dies';

        like dies { 2 - $vector1 },
            qr/operation not allowed/,
            'Scalar - Vector dies';
    };

    tests 'Division' => sub {
        my $vector1 = Popsicle::Vector->new( 10, 2 );
        my $vector2 = Popsicle::Vector->new(  5, 4 );

        is $vector1 / $vector2, object {
            prop blessed => 'Popsicle::Vector';
            call_list components => [ 2, 0.5 ];
            call      dimensions => 2;
        }, 'Vector / Vector';

        is $vector1 / 2, object {
            prop blessed => 'Popsicle::Vector';
            call_list components => [ 5, 1 ];
            call      dimensions => 2;
        }, 'Vector / Scalar';

        is $vector1 / 1, object {
            prop blessed => 'Popsicle::Vector';
            call_list components => [ 10, 2 ];
            call      dimensions => 2;
        }, 'Vector / 1';

        is $vector1 / [ 3, 4 ], object {
            prop blessed => 'Popsicle::Vector';
            call_list components => [ 10 / 3, 1 / 2 ];
            call      dimensions => 2;
        }, 'Vector / ArrayRef';

        like dies { $vector1 / undef },
            qr/Missing parameter in vector arithmetic/,
            'Vector / Undef dies';

        like dies { 2 / $vector1 },
            qr/operation not allowed/,
            'Scalar / Vector dies';

        like dies { $vector1 / 0 },
            qr/illegal division by zero in/i,
            'Vector / 0 dies';
    };

    tests 'Multiplication' => sub {
        my $vector1 = Popsicle::Vector->new( 1, 2 );
        my $vector2 = Popsicle::Vector->new( 5, 4 );

        is $vector1 * $vector2, object {
            prop blessed => 'Popsicle::Vector';
            call_list components => [ 5, 8 ];
            call      dimensions => 2;
        }, 'Vector * Vector';

        is $vector1 * 2, object {
            prop blessed => 'Popsicle::Vector';
            call_list components => [ 2, 4 ];
            call      dimensions => 2;
        }, 'Vector * Scalar';

        is $vector1 * 1, object {
            prop blessed => 'Popsicle::Vector';
            call_list components => [ 1, 2 ];
            call      dimensions => 2;
        }, 'Vector * 1';

        is $vector1 * 0, object {
            prop blessed => 'Popsicle::Vector';
            call_list components => [ 0, 0 ];
            call      dimensions => 2;
        }, 'Vector * 0';

        is $vector1 * [ 3, 4 ], object {
            prop blessed => 'Popsicle::Vector';
            call_list components => [ 3, 8 ];
            call      dimensions => 2;
        }, 'Vector * ArrayRef';

        like dies { $vector1 * undef },
            qr/Missing parameter in vector arithmetic/,
            'Vector * Undef dies';

        like dies { 2 * $vector1 },
            qr/operation not allowed/,
            'Scalar * Vector dies';
    };

    tests 'Unary minus' => sub {
        my $vector1 = Popsicle::Vector->new( 1, 2, 3, 4 );

        is -$vector1, object {
            prop blessed => 'Popsicle::Vector';
            call_list components => [ -1, -2, -3, -4 ];
            call      dimensions => 4;
        };
    };

    tests 'Unary plus' => sub {
        my $vector1 = Popsicle::Vector->new( 1, 2, 3, 4 );

        is +$vector1, object {
            prop blessed => 'Popsicle::Vector';
            call_list components => [ 1, 2, 3, 4 ];
            call      dimensions => 4;
        };
    };

    tests 'Absolute values' => sub {
        my $vector1 = Popsicle::Vector->new( -1, 2, -3, 4 );

        is abs($vector1), object {
            prop blessed => 'Popsicle::Vector';
            call_list components => [ 1, 2, 3, 4 ];
            call      dimensions => 4;
        };
    };
};

describe 'Construction' => sub {
    tests 'From string' => sub {
        is +Popsicle::Vector->from_string('1,2,3'), object {
            prop blessed => 'Popsicle::Vector';
            call_list components => [ 1, 2, 3 ];
        };
    };

    tests 'From arrayref' => sub {
        is +Popsicle::Vector->new( [ 1, 2, 3 ] ), object {
            prop blessed => 'Popsicle::Vector';
            call_list components => [ 1, 2, 3 ];
        };
    };

    tests 'From array' => sub {
        is +Popsicle::Vector->new( 1, 2, 3 ), object {
            prop blessed => 'Popsicle::Vector';
            call_list components => [ 1, 2, 3 ];
        };
    };

    tests 'From vector' => sub {
        my $vector = Popsicle::Vector->new( 1, 2, 3 );

        is +Popsicle::Vector->new( $vector ), object {
            prop blessed => 'Popsicle::Vector';
            prop this => not_in_set( exact_ref $vector );
            call_list components => [ 1, 2, 3 ];
        };
    };

    tests 'As method call' => sub {
        my $vector = Popsicle::Vector->new( 1, 2, 3 );

        is $vector->new( 2, 3, 4 ), object {
            prop blessed => 'Popsicle::Vector';
            prop this => not_in_set( exact_ref $vector );
            call_list components => [ 2, 3, 4 ];
        };
    };

    tests 'Clone' => sub {
        my $vector = Popsicle::Vector->new( 1, 2, 3 );

        is $vector->clone, object {
            prop blessed => 'Popsicle::Vector';
            prop this => not_in_set( exact_ref $vector );
            call_list components => [ 1, 2, 3 ];
        };
    };
};

describe 'Object modification' => sub {
    tests 'Lvalue accesors' => sub {
        my $vector = Popsicle::Vector->new( 1, 2, 3 );

        is $vector->x, 1, 'Read x';
        is $vector->y, 2, 'Read y';
        is $vector->z, 3, 'Read z';

        is [ $vector->components ], [ 1, 2, 3 ], 'Check initial components';

        $vector->x = 10;
        $vector->y = 11;
        $vector->z = 12;

        is $vector->x, 10, 'Set x';
        is $vector->y, 11, 'Set y';
        is $vector->z, 12, 'Set z';

        is [ $vector->components ], [ 10, 11, 12 ], 'Check end components';
    };

    tests 'Direct access' => sub {
        my $vector = Popsicle::Vector->new( 1, 2, 3 );

        is $vector->[0], 1, 'Read raw x';
        is $vector->[1], 2, 'Read raw y';
        is $vector->[2], 3, 'Read raw z';

        is [ $vector->components ], [ 1, 2, 3 ], 'Check initial components';

        $vector->[0] = 10;
        $vector->[1] = 11;
        $vector->[2] = 12;

        is $vector->[0], 10, 'Set raw x';
        is $vector->[1], 11, 'Set raw y';
        is $vector->[2], 12, 'Set raw z';

        is [ $vector->components ], [ 10, 11, 12 ], 'Check end components';
    };
};

describe 'Vector arithmetic' => sub {
    tests 'Subtraction' => sub {
        my $vector1 = Popsicle::Vector->new( 1, 4 );
        my $vector2 = Popsicle::Vector->new( 2, 8 );

        is $vector1->subtract($vector2), object {
            prop blessed => 'Popsicle::Vector';
            call_list components => [ -1, -4 ];
            call      dimensions => 2;
        }, 'Vector - Vector';

        is $vector1->subtract(2), object {
            prop blessed => 'Popsicle::Vector';
            call_list components => [ -1, 2 ];
            call      dimensions => 2;
        }, 'Vector - Scalar';

        is $vector1->subtract( [ -1, 2 ] ), object {
            prop blessed => 'Popsicle::Vector';
            call_list components => [ 2, 2 ];
            call      dimensions => 2;
        }, 'Vector - ArrayRef';

        is $vector1->subtract(0), object {
            prop blessed => 'Popsicle::Vector';
            call_list components => [ 1, 4 ];
            call      dimensions => 2;
        }, 'Vector - 0';

        like dies { $vector1->subtract(undef) },
            qr/Missing parameter in vector arithmetic/,
            'Vector - Undef dies';

        like dies { $vector1->subtract },
            qr/Missing parameter in vector arithmetic/,
            'Vector - Nothing dies';

        like dies { $vector1->subtract( [ -1, 2, 3 ] ) },
            qr/Vectors are not of the same length/,
            'Vector - ArrayRef of different length dies';

        my $vector3 = Popsicle::Vector->new( 2, 8, 16 );
        like dies { $vector1->subtract( $vector3 ) },
            qr/Vectors are not of the same length/,
            'Vector - Vector of different length dies';
    };

    tests 'Addition' => sub {
        my $vector1 = Popsicle::Vector->new( 1, 4 );
        my $vector2 = Popsicle::Vector->new( 2, 8 );

        is $vector1->add($vector2), object {
            prop blessed => 'Popsicle::Vector';
            call_list components => [ 3, 12 ];
            call      dimensions => 2;
        }, 'Vector + Vector';

        is $vector1->add(2), object {
            prop blessed => 'Popsicle::Vector';
            call_list components => [ 3, 6 ];
            call      dimensions => 2;
        }, 'Vector + Scalar';

        is $vector1->add( [ -1, 2 ] ), object {
            prop blessed => 'Popsicle::Vector';
            call_list components => [ 0, 6 ];
            call      dimensions => 2;
        }, 'Vector + ArrayRef';

        is $vector1->add(0), object {
            prop blessed => 'Popsicle::Vector';
            call_list components => [ 1, 4 ];
            call      dimensions => 2;
        }, 'Vector + 0';

        like dies { $vector1->add(undef) },
            qr/Missing parameter in vector arithmetic/,
            'Vector + Undef dies';

        like dies { $vector1->add },
            qr/Missing parameter in vector arithmetic/,
            'Vector + Nothing dies';

        like dies { $vector1->add( [ -1, 2, 3 ] ) },
            qr/Vectors are not of the same length/,
            'Vector + ArrayRef of different length dies';

        my $vector3 = Popsicle::Vector->new( 2, 8, 16 );
        like dies { $vector1->add( $vector3 ) },
            qr/Vectors are not of the same length/,
            'Vector + Vector of different length dies';
    };

    tests 'Multiplication' => sub {
        my $vector1 = Popsicle::Vector->new( 1, 4 );
        my $vector2 = Popsicle::Vector->new( 2, 8 );

        is $vector1->multiply($vector2), object {
            prop blessed => 'Popsicle::Vector';
            call_list components => [ 2, 32 ];
            call      dimensions => 2;
        }, 'Vector * Vector';

        is $vector1->multiply(2), object {
            prop blessed => 'Popsicle::Vector';
            call_list components => [ 2, 8 ];
            call      dimensions => 2;
        }, 'Vector * Scalar';

        is $vector1->multiply(1), object {
            prop blessed => 'Popsicle::Vector';
            call_list components => [ 1, 4 ];
            call      dimensions => 2;
        }, 'Vector * 1';

        is $vector1->multiply(0), object {
            prop blessed => 'Popsicle::Vector';
            call_list components => [ 0, 0 ];
            call      dimensions => 2;
        }, 'Vector * 0';

        is $vector1->multiply( [ -1, 2 ] ), object {
            prop blessed => 'Popsicle::Vector';
            call_list components => [ -1, 8 ];
            call      dimensions => 2;
        }, 'Vector * ArrayRef';

        like dies { $vector1->multiply(undef) },
            qr/Missing parameter in vector arithmetic/,
            'Vector * Undef dies';

        like dies { $vector1->multiply },
            qr/Missing parameter in vector arithmetic/,
            'Vector * Nothing dies';

        like dies { $vector1->multiply( [ -1, 2, 3 ] ) },
            qr/Vectors are not of the same length/,
            'Vector * ArrayRef of different length dies';

        my $vector3 = Popsicle::Vector->new( 2, 8, 16 );
        like dies { $vector1->multiply( $vector3 ) },
            qr/Vectors are not of the same length/,
            'Vector * Vector of different length dies';
    };

    tests 'Division' => sub {
        my $vector1 = Popsicle::Vector->new( 1, 4 );
        my $vector2 = Popsicle::Vector->new( 2, 8 );

        is $vector1->divide($vector2), object {
            prop blessed => 'Popsicle::Vector';
            call_list components => [ 0.5, 0.5 ];
            call      dimensions => 2;
        }, 'Vector / Vector';

        is $vector1->divide(2), object {
            prop blessed => 'Popsicle::Vector';
            call_list components => [ 0.5, 2 ];
            call      dimensions => 2;
        }, 'Vector / Scalar';

        is $vector1->divide(1), object {
            prop blessed => 'Popsicle::Vector';
            call_list components => [ 1, 4 ];
            call      dimensions => 2;
        }, 'Vector / 1';

        like dies { $vector1->divide(0) },
            qr/Illegal division by zero in entrywise division/,
            'Vector / 0 dies';

        like dies { $vector1->divide( [ 0, 1 ] ) },
            qr/Illegal division by zero in entrywise division/,
            'Vector / [ 0, 1 ] dies';

        is $vector1->divide( [ -1, 2 ] ), object {
            prop blessed => 'Popsicle::Vector';
            call_list components => [ -1, 2 ];
            call      dimensions => 2;
        }, 'Vector / ArrayRef';

        like dies { $vector1->divide(undef) },
            qr/Missing parameter in vector arithmetic/,
            'Vector / Undef dies';

        like dies { $vector1->divide },
            qr/Missing parameter in vector arithmetic/,
            'Vector / Nothing dies';

        like dies { $vector1->divide( [ -1, 2, 3 ] ) },
            qr/Vectors are not of the same length/,
            'Vector / ArrayRef of different length dies';

        my $vector3 = Popsicle::Vector->new( 2, 8, 16 );
        like dies { $vector1->divide( $vector3 ) },
            qr/Vectors are not of the same length/,
            'Vector / Vector of different length dies';
    };

    tests 'Rounding' => sub {
        my $vector = Popsicle::Vector->new( 0.49, 0.5, 0.51 );

        is $vector->round, object {
            prop blessed => 'Popsicle::Vector';
            prop this    => not_in_set( exact_ref $vector );
            call_list components => [ 0, 1, 1 ];
            call      dimensions => 3;
        }, '->round like calling lround';

        is $vector->floor, object {
            prop blessed => 'Popsicle::Vector';
            prop this    => not_in_set( exact_ref $vector );
            call_list components => [ 0, 0, 0 ];
            call      dimensions => 3;
        }, '->floor like calling floor';

        is $vector->ceil, object {
            prop blessed => 'Popsicle::Vector';
            prop this    => not_in_set( exact_ref $vector );
            call_list components => [ 1, 1, 1 ];
            call      dimensions => 3;
        }, '->ceil like calling ceil';
    };

    tests 'Absolute values' => sub {
        my $vector = Popsicle::Vector->new( -1, 2, -3, 4 );

        is $vector->abs, object {
            prop blessed => 'Popsicle::Vector';
            call_list components => [ 1, 2, 3, 4 ];
            call      dimensions => 4;
        };
    };
};

describe 'Length' => sub {
    tests 'Length' => sub {
        is +Popsicle::Vector->new( 3, 4 )->length,    5, '2D vector';
        is +Popsicle::Vector->new( 1, 2, 2 )->length, 3, '3D vector';
    };

    tests 'Normal vectors' => sub {
        is +Popsicle::Vector->new( 3, 4 )->normalise, object {
            call      length     => 1;
            call_list components => [ 0.6, 0.8 ];
        }, 'Normalise 2D vector';

        is +Popsicle::Vector->new( 1, 2, 2 )->normalise, object {
            call      length     => 1;
            call_list components => array {
                item within 1 / 3;
                item within 2 / 3;
                item within 2 / 3;
                end;
            };
        }, 'Normalise 3D vector';

        is +Popsicle::Vector->new( 0, 0 )->normalise, object {
            call      length     => 0;
            call_list components => [ 0, 0 ];
        }, 'Normalise zero vector returns zero vector';
    };
};

tests 'Reverse' => sub {
    is +Popsicle::Vector->new( 3, 4 )->reverse, object {
        call_list components => [ 4, 3 ];
    }, 'Reverses components in 2D vector';

    is +Popsicle::Vector->new( 1, 2, 2 )->reverse, object {
        call_list components => array {
            item within 2;
            item within 2;
            item within 1;
            end;
        };
    }, 'Reverses 3D vector';
};

tests 'Zero' => sub {
    is +Popsicle::Vector->new( 3, 4 )->zero, object {
        call_list components => [ 0, 0 ];
    }, 'Sets 2D vector to zero';

    is +Popsicle::Vector->new( 1, 2, 2 )->zero, object {
        call_list components => [ 0, 0, 0 ];
    }, 'Sets 3D vector to zero';
};

tests 'Slope' => sub {
    is +Popsicle::Vector->new( 2, 4 )->slope, object {
        prop blessed => 'Popsicle::Slope';
        prop this    => [ 2, 4 ];
        call value   => 2;
    }, 'Returns the slope of a vector';
};

done_testing;
