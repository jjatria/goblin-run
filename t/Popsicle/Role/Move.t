use Test2::V0;
use Test2::Tools::Spec;

tests 'Speed and velocity' => sub {
    is My::Class->new, object {
        call velocity => object {
            prop blessed => 'Popsicle::Vector';
            call_list components => [ 0, 0 ];
        };
        call speed => 0;
    }, 'Velocity defaults to 0';

    my $entity = My::Class->new(
        position => [ 10, 7 ],
        velocity => [ -1, 1 ],
    );

    is $entity->speed, $entity->velocity->length,
        'Speed is the magnitude of velocity';

    is $entity, object {
        call velocity => object {
            prop blessed => 'Popsicle::Vector';
            call_list components => [ -1, 1 ];
        };
    }, 'Velocity from constructor';

    $entity->dx = 5;
    $entity->dy = 3;

    is $entity->velocity, [ 5, 3 ], 'Velocity has lvalue accessors';

    $entity->velocity([ 4, 2 ]);

    is $entity->velocity, object {
        call_list components => [ 4, 2 ];
    }, 'Velocity is read-write';
};

tests 'Higher dimensions' => sub {
    is My::Class->new( position => [ 10, 7, 5 ] ), object {
        call speed    => 0;
        call velocity => object {
            prop blessed => 'Popsicle::Vector';
            call_list components => [ 0, 0, 0 ];
        };
    }, 'Velocity defaults to the same dimensions as position';
};

tests 'Move' => sub {
    my $entity = My::Class->new(
        position => [  0, 0 ],   # At origin
        velocity => [ -1, 1 ],   # Moving south-west
    );

    is $entity, object {
        call velocity => [ -1, 1 ];
        call speed    => rounded 1.414, 3;
    }, 'Take a double-step';

    is $entity->move,
        object { call position => [ -1, 1 ] },
        'Takes full step by default';

    is $entity->move(0.5),
        object { call position => [ -1.5, 1.5 ] },
        'Take a half-step';

    is $entity->move(2),
        object { call position => [ -3.5, 3.5 ] },
        'Take a double-step';

    $entity->stop_moving;

    is $entity, object {
        call velocity => [ 0, 0 ];
        call speed    => 0;
    }, 'Stops moving';

    is $entity->move(2),
        object { call position => [ -3.5, 3.5 ] },
        'Does not move without speed';
};

package My::Class {
    use Moo;
    with 'Popsicle::Role::Move';
};

done_testing;
