use Test2::V0;
use Test2::Tools::Spec;

tests 'Position' => sub {
    is My::Class->new, object {
        call position => object {
            prop blessed => 'Popsicle::Vector';
            call_list components => [ 0, 0 ];
        };
    }, 'Default position';

    my $entity = My::Class->new( position => [ 10, 7 ] );

    is $entity, object {
        call position => object {
            prop blessed => 'Popsicle::Vector';
            call_list components => [ 10, 7 ];
        };
    }, 'Position from constructor';

    $entity->x = 5;
    $entity->y = 3;

    is $entity->position, [ 5, 3 ], 'Has lvalue accessors';

    $entity->position([ 4, 2 ]);

    is $entity->position, object {
        call_list components => [ 4, 2 ];
    }, 'Position is read-write';
};

package My::Class {
    use Moo;
    with 'Popsicle::Role::Position';
};

done_testing;
