use Test2::V0;
use Test2::Tools::Spec;

tests 'Size' => sub {
    is My::Box->new, object {
        call area   => 0;
        call width  => 0;
        call height => 0;
        call centre => object {
            prop blessed => 'Popsicle::Vector';
            call_list components => [ 0, 0 ];
        };
        call dimensions => object {
            prop blessed => 'Popsicle::Vector';
            call_list components => [ 0, 0 ];
        };
    }, 'Default dimensions';

    my $entity = My::Box->new( dimensions => [ 10, 7 ] );

    is $entity, object {
        call dimensions => object {
            prop blessed => 'Popsicle::Vector';
            call_list components => [ 10, 7 ];
        };
    }, 'Size from constructor';

    $entity->width  = 5;
    $entity->height = 3;

    is $entity->dimensions, [ 5, 3 ], 'Has lvalue accessors';

    $entity->dimensions([ 4, 2 ]);

    is $entity->dimensions, object {
        call_list components => [ 4, 2 ];
    }, 'Size is read-write';

    is $entity->centre, [ 2, 1 ], 'Calculates centre';

    my $pos = My::Pos->new( position => [ 3, 3 ], dimensions => [ 10, 10 ] );
    is $pos->centre, [ 8, 8 ], 'Calculates centre aware of position';
};

tests 'Bounds' => sub {
    is My::Box->new( dimensions => [ 10, 7 ] ), object {
        call [ in_bounds => [ 10,  7 ] ] => T;
        call [ in_bounds => [  0,  0 ] ] => T;
        call [ in_bounds => [ -1,  0 ] ] => F;
        call [ in_bounds => [ 10,  8 ] ] => F;
        call [ in_bounds => [ 10, -7 ] ] => F;
    }, 'Test bounds from origin';

    is My::Pos->new( dimensions => [ 3, 2 ], position => [ 6, 5 ] ),
        object {
            call [ in_bounds => [ 9, 7 ] ] => T;
            call [ in_bounds => [ 6, 5 ] ] => T;
            call [ in_bounds => [ 2, 3 ] ] => F;
            call [ in_bounds => [ 0, 0 ] ] => F;
            call [ in_bounds => [ 5, 8 ] ] => F;
        },
        'Test bounds from position if available';
};

tests 'Overlaps' => sub {
    my %size = ( dimensions => [ 3, 3 ] );
    my $box = My::Pos->new( %size, position => [ 5, 5 ] );

    is $box->overlaps_with( My::Pos->new( %size, position => [ 5, 2 ] )), F,
        'Box does not overlap with box right above it';

    is $box->overlaps_with( My::Pos->new( %size, position => [ 5, 3 ] )), T,
        'Box overlaps with box above';

    is $box->overlaps_with( My::Pos->new( %size, position => [ 5, 8 ] )), F,
        'Box does not overlap with box right below it';

    is $box->overlaps_with( My::Pos->new( %size, position => [ 7, 5 ] )), T,
        'Box overlaps with box below';

    is $box->overlaps_with( My::Pos->new( %size, position => [ 8, 5 ] )), F,
        'Box does not overlap with box right to the right';

    is $box->overlaps_with( My::Pos->new( %size, position => [ 7, 5 ] )), T,
        'Box overlaps with box on the right';

    is $box->overlaps_with( My::Pos->new( %size, position => [ 2, 5 ] )), F,
        'Box does not overlap with box right to the left';

    is $box->overlaps_with( My::Pos->new( %size, position => [ 3, 5 ] )), T,
        'Box overlaps with box on the left';
};

package My::Box {
    use Moo;
    with qw( Popsicle::Role::Dimensions );
};

package My::Pos {
    use Moo;
    with qw(
        Popsicle::Role::Dimensions
        Popsicle::Role::Position
    );
};

done_testing;
