requires 'Graph::Base',        '0.001006';
requires 'Games::TMX::Parser', '1.000000';

requires 'Beam::Emitter',               '1.007';
requires 'Class::Load',                 '0.25';
requires 'Exporter::Tiny',              '1.002001';
requires 'Hash::Ordered',               '0.014';
requires 'JSON::MaybeXS',               '1.004000';
requires 'Log::Any',                    '1.707';
requires 'Log::Any::Adapter::Log4perl', '0.09';
requires 'Log::Dispatch',               '2.68';
requires 'Log::Log4perl',               '1.49';
requires 'Moo',                         '2.003004';
requires 'Path::Tiny',                  '0.108';
requires 'Ref::Util',                   '0.204';
requires 'SDL',                         '2.548';
requires 'Type::Tiny',                  '1.004004';
requires 'curry',                       '1.001000';
requires 'namespace::clean',            '0.27';

on test => sub {
    requires 'Test2::Harness' => '0.001080';
    requires 'Devel::Cover'   => '1.33';
};
