package Local::LevelBuilder::ConnectedRooms;

use Moo;

use Popsicle::Map::Grid;
use Graph::Base::Heap;
use Graph::Base::Grid::Square;
use Popsicle::Types 'Vector';

use Log::Any '$log';
with qw( Popsicle::Role::Emitter );

use namespace::clean;

has merge_chance => ( is => 'ro', default => 0.25 );
has max_rooms    => ( is => 'ro', default => 20 );

sub generate {
    my ( $self, %params ) = @_;

    my $grid = Popsicle::Map::Grid->new(
        dimensions => delete $params{dimensions},
        lazy       => 1,
        complete   => 0,
    );

    my $heap = Graph::Base::Heap->new;

    do {
        $self->add_room( $grid, $heap );
    } until ! @{ $heap->peek->directions };

    return $grid;
}

sub add_room {
    my ( $self, $grid, $heap ) = @_;

    my $room = $heap->poll;

    my ( $new, $door, $merge );

    if ($room) {
        $log->warnf('We have a room: %s -> %s', $room->position, $room->dimensions );

        $log->warnf('Room has %s directions left: %s',
            scalar @{ $room->directions }, join ', ', @{ $room->directions } );

        while ( my $dir = shift @{ $room->directions } ) {
            $log->warnf('Facing %s', $dir);

            $door = find_door( $grid, $room, $dir, 2 );

            unless ($door) {
                $log->warn('Couldnt find a door!');
                next;
            }

            $log->warnf('Possible door at: %s', $door );

            $grid->lock;
            my $cell = $grid->get_cell( $door + $dir );
            $grid->unlock;

            if ( $cell && $cell->is_passable ) {
                $log->warn('There is floor on the other side! Just open the door');
                add_door( $grid, $door );
                last;
            }

            if ( $heap->count >= $self->max_rooms ) {
                $log->warn('Already at maximum number of rooms');
                next;
            }

            $log->warn('There is nothing on the other side! Carve a room');
            $new = new_room( $door, $dir );

            $log->warnf('Trying to place a room at: %s -> %s', $new->position, $new->dimensions );

            unless ( is_room_in_bounds( $grid, $new ) ) {
                undef $new;
                next;
            }

            if ( is_room_placeable( $grid, $new ) ) {
                last;
            }
            elsif ( rand >= $self->merge_chance ) {
                $log->warn('Merge room!');
                $merge = 1;
                last;
            }
            else {
                $log->warn('Cannot place room!');
                undef $new;
            }
        }
    }
    else {
        $log->warn('We have no room');
        $new = new_room(
            { position => ( Vector->coerce($grid->dimensions) / 2 )->round }
        );
    }

    if ($new) {
        $heap->add([ scalar @{ $new->directions } * -1, $new ]);

        if ($merge) {
            merge_room_to_grid( $grid, $new );
        }
        else {
            apply_room_to_grid( $grid, $new );
        }

        add_door( $grid, $door ) if $door;
    }

    $heap->add([ scalar @{ $room->directions } * -1, $room ]) if $room;

    return;
}

sub add_door {
    my ( $grid, $coord ) = @_;
    $log->warnf('Adding a door at %s', $coord);
    $grid->get_cell($coord)->make_passable;
}

sub find_door {
    my ( $grid, $room, $wall, $min ) = @_;

    my ( $x, $y );

    my @checks;

    # North / South
    if ( $wall->x == 0 ) {
        $x = int( rand $room->width );
        $y = $wall->y > 0 ? $room->height : -1;

        @checks = map {
            [ $x - $_, $y ], [ $x + $_, $y ]
        } glob '{' . join( ',', 1 .. $min-1 ) . '}'
    }

    # East / West
    else {
        $y = int( rand $room->height );
        $x = $wall->x > 0 ? $room->width : -1;

        @checks = map {
            [ $x, $y - $_ ], [ $x, $y + $_ ]
        } glob '{' . join( ',', 1 .. $min-1 ) . '}'
    }

    $log->warnf('Possible door at %s', $room->position + [ $x, $y ]);

    my $good = 1;

    for (@checks) {
        my $coord = $room->position + $_;

        $grid->lock;
        my $cell = $grid->get_cell($coord);
        $grid->unlock;

        if ( $cell && $cell->is_passable ) {
            $good = 0;
            last;
        }
    }

    return $room->position + [ $x, $y ] if $good;

    return;
}

sub new_room {
    my $opts;
    $opts = pop @_ if ref $_[-1] eq 'HASH';

    my ( $door, $wall ) = ( shift, shift );

    my $room = My::Box->new(%{ $opts // {} });

    return $room unless $door && $wall;

    my ( $x, $y );

    # North / South
    if ( $wall->x == 0 ) {
        $y = $door->y + $wall->y;
        $y -= ( $room->height - 1 ) if $wall->y < 0;

        $x = $door->x - int( rand $room->width );
    }

    # East / West
    else {
        $x = $door->x + $wall->x;
        $x -= ( $room->width -1 ) if $wall->x < 0;

        $y = $door->y - int( rand $room->height );
    }

    $room->position([ $x, $y ]);

    $log->warnf('Adding new room: %s -> %s', $room->position, $room->dimensions );

    return $room;
}

sub is_room_in_bounds {
    my ( $grid, $room ) = @_;
    return unless $grid->in_bounds( $room->position - 1);
    return unless $grid->in_bounds( $room->position + $room->dimensions );
    return 1;
}

sub is_room_placeable {
    my ( $grid, $room ) = @_;

    my $placeable = 1;

    $grid->lock;

    my $position = $room->position;
    ATTEMPT: for my $x ( -1 .. $room->width ) {
        for my $y ( -1 .. $room->height ) {
            my $coord = $position + [ $x, $y ];

            my $cell = $grid->get_cell( $coord );
            if ( $cell && $cell->is_passable ) {
                $placeable = 0;
                last ATTEMPT;
            }
        }
    }

    $grid->unlock;

    return $placeable;
}

sub apply_room_to_grid {
    my ( $grid, $room ) = @_;

    my $position = $room->position;
    for my $x ( 0 .. $room->width-1 ) {
        for my $y ( 0 .. $room->height-1 ) {
            my $coord = $position + [ $x, $y ];
            my $cell = $grid->add_cell( $coord );
            $log->warnf('Add floor at %s', $coord);
            $cell->make_passable;

#             $grid->lock;
            $grid->connect_to_neighbours($cell);
#             $grid->unlock;
        }
    }

    for my $x ( -1 .. $room->width ) {
        for my $y ( -1, $room->height ) {
            my $coord = $position + [ $x, $y ];
            next unless $grid->in_bounds( $coord );

            $log->warnf('Add wall at %s', $coord);
            my $cell = $grid->add_cell( $coord );
            $cell->make_impassable;
        }
    }

    for my $x ( -1, $room->width ) {
        for my $y ( 0 .. $room->height-1 ) {
            my $coord = $position + [ $x, $y ];
            next unless $grid->in_bounds( $coord );

            $log->warnf('Add wall at %s', $coord);
            my $cell = $grid->add_cell( $coord );
            $cell->make_impassable;
        }
    }
}

sub merge_room_to_grid {
    my ( $grid, $room ) = @_;

    my $position = $room->position;
    for my $x ( 0 .. $room->width-1 ) {
        for my $y ( 0 .. $room->height-1 ) {
            my $coord = $position + [ $x, $y ];
            my $cell = $grid->add_cell( $coord );

            $log->warnf('Add floor at %s', $coord);

            $cell->make_passable;

            $grid->lock;
            $grid->connect_to_neighbours($cell);
            $grid->unlock;
        }
    }

    for my $x ( -1, $room->width ) {
        for my $y ( -1 .. $room->height ) {
            my $coord = $position + [ $x, $y ];
            next unless $grid->in_bounds( $coord );

            $log->warnf('Perimeter at %s', $coord);

            $grid->lock;
            my $cell = $grid->get_cell($coord);
            $grid->unlock;

            if ($cell) {
                $log->warnf('There is already a cell');
                # This is a cell that has already been added

                # Do not put a wall on a cell that is already floor
                if ( $cell->is_passable ) {
                    $log->warnf('Cell was already floor');
                    next;
                }

                $log->warnf('Make wall');
                $cell->make_impassable;
            }
            else {
                $log->warnf('There was no cell');
                # This is a new cell
                $cell = $grid->add_cell( $coord );

                # Add new walls
                $log->warnf('Make wall');
                $cell->make_impassable;
            }
        }
    }

    for my $x ( 0 .. $room->width-1 ) {
        for my $y ( -1, $room->height ) {
            my $coord = $position + [ $x, $y ];
            next unless $grid->in_bounds( $coord );

            $log->warnf('Perimeter at %s', $coord);

            $grid->lock;
            my $cell = $grid->get_cell($coord);
            $grid->unlock;


            if ($cell) {
                $log->warnf('There is already a cell');

                if ( $cell->is_passable ) {
                    $log->warnf('Cell was already floor');
                    next;
                }

                $log->warnf('Make wall');
                $cell->make_impassable;
            }
            else {
                $log->warnf('There was no cell');
                # This is a new cell
                $cell = $grid->add_cell( $coord );

                # Add new walls
                $log->warnf('Make wall');
                $cell->make_impassable;
            }
        }
    }
}

package My::Box {
    use Popsicle::Types 'Vector';
    use List::Util 'shuffle';

    use Moo;
    with qw(
        Popsicle::Has::Position
        Popsicle::Has::Dimensions
    );

    has '+dimensions' => (
        default => sub { [ 3 + int( rand 5 ), 3 + int( rand 5 ) ] },
    );

    has 'directions' => (
        is => 'ro',
        default => sub {
            [
                map Vector->coerce($_),
                shuffle @{ Graph::Base::Grid::Square->directions }
            ]
        },
    );
};

1;
