package Local::Role::Actor;

use Moo::Role;

with qw(
    Local::Role::Sprite
    Popsicle::Role::Handles::Move
    Popsicle::Role::Move
);

use namespace::autoclean;

has walking_speed  => ( is => 'ro', default => 1 );

has path => ( is => 'rw', predicate => 1, clearer => 1 );

has sight_radius => ( is => 'rw', default => 10 );

1;
