package Local::Role::Sprite;

use Moo::Role;

with qw(
    Popsicle::Role::Handles::Show
    Popsicle::Role::Position
);

use namespace::clean;

has sprite => ( is => 'ro', required => 1 );

sub draw {
    my ( $self, $stage, $surface ) = @_;

    return unless $surface;
    return unless $self->position;

    my ( $scale, $tile_width, $tile_height )
        = map { $stage->$_ } qw( scale tile_width tile_height );

    my $xy = $self->position;
    $xy -= $stage->viewport->position if $self->position_is_relative;

    $self->sprite->draw_xy(
        $surface,
        $xy->x * $tile_width  * $scale,
        $xy->y * $tile_height * $scale,
    );
}

1;
