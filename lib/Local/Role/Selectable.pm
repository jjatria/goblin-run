package Local::Role::Selectable;

use Moo::Role;

requires 'on_select';
requires 'on_deselect';

use namespace::clean;

1;
