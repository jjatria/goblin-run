package Local::Marker;

use Moo;
use Local::Helpers qw( load_sprite );

with 'Local::Role::Sprite';

use namespace::clean;

has '+sprite' => (
    lazy => 1,
    default => sub {
        load_sprite(
            image     => 'assets/marker.png',
            animated  => 1,
            clip => [ 0, 0, 32, 32 ],
            autostart => 1,
            sequence  => 'active',
            type      => 'circular',
            ticks_per_frame => 10,
            sequences => {
                active => [ [ 1, 0 ] , [ 0, 0 ] ],
            },
        ),
    }
);

sub on_show {
    my ( $self, $show ) = @_;
    $self->draw( $show->stage, $show->app );
};

sub on_event  {
    my ( $self, $event ) = @_;

    use SDL::Events qw( :type );

    if ( $event->type == SDL_MOUSEBUTTONUP ) {
        $self->emit( click => $event ) if $self->active;
        $self->active(0);
        $self->redraw( $event->stage );
    }
    elsif ( $event->type == SDL_MOUSEBUTTONDOWN ) {
        $self->active(1);
        $self->redraw( $event->stage );
    }
};

sub redraw {
    my ( $self, $stage ) = @_;
    return unless $stage;
    my $coord = $self->position or return;
    $stage->redraw($coord);
}

1;
