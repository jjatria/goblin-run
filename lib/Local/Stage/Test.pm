package Local::Stage::Test;

use Moo;
with qw(
    Popsicle::Component::Stage
);

use Carp 'croak';
use Log::Any '$log';

use Local::UI::Box;
use Popsicle::Map::Grid;
use Popsicle::Map;
use Popsicle::UI::HBox;
use Popsicle::UI::VBox;

use namespace::clean;

has '+dimensions' => (
    lazy => 1,
    default => sub {
        my $self = shift;
        $self->to_stage_coordinates( $self->game->dimensions );
    }
);

has [qw( tile_width tile_height )] => (
    is => 'ro',
    default => 1,
);

has window => (
    is => 'ro',
    default => sub {
        Popsicle::UI::HBox->new(
            dimensions => shift->dimensions,
            margin => 0,
        );
    },
);

sub BUILD {
    my $self = shift;

    my $add_box = sub {
        my $container = shift;

        my $box = Local::UI::Box->new( flex => 1, @_ );

        $box->on( enter => sub {
            $log->tracef('Entered box');
        });

        $box->on( leave => sub {
            $log->tracef('Left box');
        });

        $box->on( click => sub {
            $log->tracef('Clicked box');
        });

        $container->add_child($box);
        $self->add_entity($box);
    };

    $log->tracef('Dim [%s]', $self->game->dimensions);

    my $window = $self->window;

    my $left = Popsicle::UI::VBox->new( flex => 1 );
    $window->add_child($left);

    my $margin = 20;

    $add_box->( $left, left_margin => $margin, top_margin    => $margin);
    $add_box->( $left, left_margin => $margin, );
    $add_box->( $left, left_margin => $margin, bottom_margin => $margin);

    my $centre = Popsicle::UI::VBox->new( flex => 1 );
    $window->add_child($centre);

    $add_box->( $centre, top_margin    => $margin);
    $add_box->( $centre, margin => $margin);
    $add_box->( $centre, bottom_margin => $margin);

    my $right = Popsicle::UI::VBox->new( flex => 1 );
    $window->add_child($right);

    $add_box->( $right, right_margin => $margin, top_margin    => $margin);
    $add_box->( $right, right_margin => $margin, );
    $add_box->( $right, right_margin => $margin, bottom_margin => $margin);

    $self->on( event => sub {
        my ( $event ) = @_;

        use SDL::Events qw( :keysym );

        return unless $event->isa('Popsicle::Event::Key::Pressed');
        return unless $event->symbol == SDLK_q;

        $log->warn('Bye!');
        exit;
    });

    # Window resizing
    $self->on( event => sub {
        my ( $event ) = @_;

        return unless $event->isa('Popsicle::Event::Window::Resize');

        $log->infof('Window resized to [%s]', $event->dimensions);

        $self->game->app->resize(@{ $event->dimensions });
        $self->dimensions($event->dimensions);
        $self->window->dimensions($event->dimensions);
        $self->window->calculate_layout;
    });

    $self->on( event => sub {
        my ( $event ) = @_;

        return unless $event->isa('Popsicle::Event::Mouse::Wheel');

        $log->infof('Mouse wheel %s', $event->offset > 0 ? 'up' : 'down');
    });
}


1;
