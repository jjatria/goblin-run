package Local::Stage::Start;

use Moo;
with qw( Popsicle::Component::Stage );

use curry;
use Log::Any '$log';
use Local::UI::Button;
use Popsicle::UI::VBox;

use namespace::clean;

has '+dimensions' => (
    lazy => 1,
    default => sub {
        my $self = shift;
        $self->to_stage_coordinates( $self->game->dimensions );
    }
);

has window => (
    is => 'ro',
    default => sub {
        Popsicle::UI::VBox->new(
            dimensions => shift->dimensions,
        );
    },
);

sub BUILD {
    my $self = shift;

    my $start = Local::UI::Button->new(
        flex => 1,
        text => 'Start game!',
    );

    $start->on(
        click => $self->$curry::weak( sub { shift->game->load_stage('Cave') } )
    );

    my $quit = Local::UI::Button->new(
        flex => 1,
        text => 'Quit',
    );

    $quit->on( click => sub { exit } );

    $self->window->add_child($start);
    $self->window->add_child($quit);

    $self->add_entity($start);
    $self->add_entity($quit);
}

1;
