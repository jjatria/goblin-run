package Local::Stage::Cave;

use curry;

use Moo;
use Carp 'croak';

use Log::Any '$log';

extends 'Local::Stage';

has '+cursor' => (
    default => sub {
        my $self = shift;

        require Local::Cursor::Tiled;
        my $cursor = Local::Cursor::Tiled->new;
        $cursor->on( click => $self->curry::weak::on_click );

        return $cursor;
    },
);

has selection => (
    is        => 'rw',
    clearer   => 1,
    predicate => 1,
    trigger => sub {
        my ( $self, $val ) = @_;
        $log->debugf( 'Selection is now %s', $val ? ref($val) : 'undefined' );
    },
);

sub select_entity {
    my ( $self, $entity ) = @_;

    $self->deselect_entity;
    return unless $entity;

    unless ( $entity->DOES('Local::Role::Selectable') ) {
        $log->warnf('Tried to select an unselectable entity: %s', "$entity");
        return;
    }

    use Local::Marker;
    $self->{marker} = Local::Marker->new( position => $entity->position );

    $self->selection($entity);
    $entity->on_select($self);
    $self->add_entity($self->{marker});
}

sub deselect_entity {
    my ( $self ) = @_;

    # If we have no selection, there's nothing to do
    my $entity = $self->selection or return;

    unless ( $entity->DOES('Local::Role::Selectable') ) {
        $log->warnf('Tried to de-select an unselectable entity: %s', "$entity");
        return;
    }

    $entity->on_deselect($self);
    $self->remove_entity( delete $self->{marker} ) if $self->{marker};
    $self->clear_selection;
}

sub BUILD {
    my $self = shift;

    # Hides the system cursor
    $self->hide_cursor;

    use Local::Actor::Main;
    my $main = Local::Actor::Main->new;
    $self->add_entity($main);
    $self->add_entity($self->cursor);
}

sub on_click {
    my ( $self, $event ) = @_;

    my $cursor = $event->emitter;

    my $coord = $cursor->position;
    $coord += $event->stage->viewport->position
        unless $cursor->position_is_relative;

    return unless $coord;
    $log->debugf( 'Cursor clicked at [%s]', $coord );

    use SDL::Events;

    $log->tracef( 'Button Right: %s', SDL_BUTTON_RIGHT );
    $log->tracef( 'Button: %s', $event->button );

    if ( $event->button == SDL_BUTTON_RIGHT ) {
        # Right click
        my $selection = $self->selection or return;

        my $breadcrumbs = $selection->breadcrumbs or return;

        my $path = $breadcrumbs->to_path( $self->map->grid );

        if ($path) {
            $log->trace( $path->dump );
            $selection->path( $path );

            $self->remove_entity( delete $self->{marker} ) if $self->{marker};
            $self->remove_entity( $breadcrumbs );
            $selection->clear_breadcrumbs;
        }
    }
    elsif ( $event->button == SDL_BUTTON_LEFT ) {
        $self->deselect_entity;
    }
};

1;
