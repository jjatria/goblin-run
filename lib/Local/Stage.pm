package Local::Stage;

use Moo;
with qw( Popsicle::Component::Stage );

use Log::Any '$log';

use Local::Helpers qw( load_sprite );
use Popsicle::Helpers qw( map_from_tmx );
use Popsicle::Types qw( Map Vector );

use namespace::clean;

has config   => ( is => 'ro', default => sub { +{} } );
has scale    => ( is => 'rw', default => 1 );

has cursor => ( is => 'ro' );

has viewport => (
    is => 'ro',
    lazy => 1,
    default => sub {
        require Popsicle::Component::Viewport;
        Popsicle::Component::Viewport->new(
            position   => [ 0, 0 ],
            dimensions => [ $_[0]->tiles_wide, $_[0]->tiles_high ],
        );
    },
);

has map => (
    is => 'ro',
    isa => Map,
    lazy => 1,
    default => sub { map_from_tmx(%{ shift->config->{map} }) },
    handles => {
        tiles_high  => 'height',
        tiles_wide  => 'width',
        tile_height => 'tile_height',
        tile_width  => 'tile_width',
    },
);

has needs_redraw => (
    is       => 'ro',
    lazy     => 1,
    init_arg => undef,
    clearer  => 'reset_stage',
    default  => sub {
        my ($self) = @_;

        my %map;

        for my $x ( 0 .. $_[0]->tiles_wide ) {
            for my $y ( 0 .. $_[0]->tiles_high ) {
                $map{"$x,$y"} = 0;
            }
        }

        return \%map;
    },
);

around BUILDARGS => sub {
    my $orig = shift;
    my $self = shift;

    my $args = $self->$orig(@_);

    my $code = $self =~ s/^Local::Stage:://r;

    if (!$args->{map}) {
        my $config = $args->{config}{stages}{$code};

        if ($config) {
            require Games::TMX::Parser;

            my $map = Games::TMX::Parser->new(
                map_dir  => $config->{map}{root},
                map_file => $config->{map}{filename},
            )->map;


            $args->{map} = $map;
        }
    }

    return $args;
};

sub BUILD {
    my ( $self ) = @_;
    $self->load_sprites;

    $self->on( show => sub {
        my ( $show ) = @_;

        my $redraw = $self->needs_redraw;
        return unless %{ $redraw };

        my $map  = $self->map;
        my $view = $self->viewport;

        my @layers = @{ $map->layers };
        for my $x ( 0 .. $view->width ) {
            for my $y ( 0 .. $view->height ) {
                my $camera = Vector->coerce([ $x, $y ]);
                my $world  = $view->position + $camera;

                my $index = delete $redraw->{$world} // 0;

                for my $i ( $index .. $#layers ) {
                    my $layer = $layers[$i];
                    my $cell = $layer->get_cell($world) or next;
                    my $tile = $cell->tile or next;

                    # FIXME Why does this get deleted?
                    my $clip = $tile->{sprite}->clip;
                    $clip->w($self->tile_width);
                    $clip->h($self->tile_height);

                    $tile->{sprite}->draw_xy(
                        $show->app,
                        $x * $self->tile_width,
                        $y * $self->tile_height,
                    );
                }
            }
        }
    });

    # Temporarily move the viewport with arrow keys
    $self->on( event => sub {
        my ( $event ) = @_;

        return unless $event->isa('Popsicle::Event::Key::Pressed');

        use SDL::Events qw( :keysym );

        for ( $event->symbol ) {
            my $delta;

            if ( $_ == SDLK_UP ) { $delta = [ 0, -1 ] }
            elsif ( $_ == SDLK_DOWN ) { $delta = [ 0, 1 ] }
            elsif ( $_ == SDLK_LEFT ) { $delta = [ -1, 0 ] }
            elsif ( $_ == SDLK_RIGHT ) { $delta = [ 1, 0 ] }

            last unless $delta;

            $self->reset_stage;
            my $view = $self->viewport;

            $view->position( $view->position + $delta );
        }
    });

    # Window resizing
    $self->on( event => sub {
        my ( $event ) = @_;

        return unless $event->isa('Popsicle::Event::Window::Resize');

        $log->infof('Window resized to [%s]', $event->dimensions);
    });

    $self->hide_cursor;
}

sub load_sprites {
    my ($self) = @_;

    my $map = $self->map;
    for my $tileset ( @{ $map->tilesets } ) {
        my $image = $tileset->image;

        for my $tile ( @{ $tileset->tiles } ) {
            next if $tile->{sprite};

            $tile->{sprite} = load_sprite(
                image   => $image,
                clip    => [
                    $self->tile_width  * $tile->x,
                    $self->tile_height * $tile->y,
                    $self->tile_width,
                    $self->tile_height,
                ],
            );
        }
    }
}

sub to_stage_coordinates {
    my ( $self, $coord, $relative ) = @_;

    # Convert mouse position to tile position
    my $xy = $coord / [ $self->tile_width, $self->tile_height ];

    # Tile position is relative to viewport
    $xy += $self->viewport->position if $relative;

    return $xy;
}

sub redraw {
    my ( $self, $coord, $level ) = @_;

    $coord = Vector->coerce($coord) unless Vector->check($coord);

    $self->needs_redraw->{ "$coord" } = $level // 0;
}

1;
