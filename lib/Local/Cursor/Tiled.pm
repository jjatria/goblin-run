package Local::Cursor::Tiled;

use Moo;

extends 'Local::Cursor';
with qw( Popsicle::Role::Emitter );

use namespace::clean;

after on_event => sub {
    my ( $self, $event ) = @_;

    use SDL::Events qw( :type );

    return unless $event->isa('Popsicle::Event::Mouse::Motion');

    my $stage = $event->stage;
    $self->redraw($stage);

    my $pos = $event->position / [ $stage->tile_width, $stage->tile_height ];
    $self->position($pos->floor);
};

sub redraw {
    my ( $self, $stage ) = @_;
    return unless $stage;

    my $coord = $self->position or return;
    $coord += $stage->viewport->position unless $self->position_is_relative;

    $stage->redraw($coord);
}

1;
