package Local::BreadCrumbs;

use Moo;
use Types::Standard qw( HashRef );
use Popsicle::Types qw( Vector );
use Local::Helpers qw( load_sprite );
use SDL::Events;

with qw(
    Popsicle::Role::Handles::Event
    Popsicle::Role::Handles::Show
);

has start => ( is => 'ro', required => 1, isa => Vector, coerce => 1 );
has end   => ( is => 'rw', clearer => 1,  isa => Vector, coerce => 1 );

has path_map => ( is => 'ro', isa => HashRef, required => 1 );

use constant FILE => 'assets/arrows.png';

my %SPRITES = (
    OUT_N  => load_sprite( image => FILE, clip => [  0,   0, 32, 32 ] ),
    OUT_NE => load_sprite( image => FILE, clip => [  0,  32, 32, 32 ] ),
    OUT_E  => load_sprite( image => FILE, clip => [  0,  64, 32, 32 ] ),
    OUT_SE => load_sprite( image => FILE, clip => [  0,  96, 32, 32 ] ),
    OUT_S  => load_sprite( image => FILE, clip => [  0, 128, 32, 32 ] ),
    OUT_SW => load_sprite( image => FILE, clip => [  0, 160, 32, 32 ] ),
    OUT_W  => load_sprite( image => FILE, clip => [  0, 192, 32, 32 ] ),
    OUT_NW => load_sprite( image => FILE, clip => [  0, 224, 32, 32 ] ),
    IN_N   => load_sprite( image => FILE, clip => [ 32,   0, 32, 32 ] ),
    IN_NE  => load_sprite( image => FILE, clip => [ 32,  32, 32, 32 ] ),
    IN_E   => load_sprite( image => FILE, clip => [ 32,  64, 32, 32 ] ),
    IN_SE  => load_sprite( image => FILE, clip => [ 32,  96, 32, 32 ] ),
    IN_S   => load_sprite( image => FILE, clip => [ 32, 128, 32, 32 ] ),
    IN_SW  => load_sprite( image => FILE, clip => [ 32, 160, 32, 32 ] ),
    IN_W   => load_sprite( image => FILE, clip => [ 32, 192, 32, 32 ] ),
    IN_NW  => load_sprite( image => FILE, clip => [ 32, 224, 32, 32 ] ),
);

sub on_event {
    my ( $self, $event ) = @_;

    if ( $event->isa('Popsicle::Event::Mouse::Motion') ) {
        my $stage = $event->stage;
        $self->redraw($stage);

        # Convert mouse position to tile position
        my $xy = $event->position / [ $stage->tile_width, $stage->tile_height ];

        # Tile position is relative to viewport
        $xy += $stage->viewport->position;

        $self->end($xy->floor);
    }
}

sub on_show {
    my ( $self, $show ) = @_;

    my $leads_to = $self->path_map or return;

    my $stage = $show->stage;
    my $start = $self->start;
    my $here  = $self->end or return;

    my ( $scale, $tile_width, $tile_height )
        = map { $stage->$_ } qw( scale tile_width tile_height );

    for (values %SPRITES) {
        my $clip = $_->clip;
        $clip->w($tile_width);
        $clip->h($tile_height);
    }

    my $view = $stage->viewport->position;

    my $prev;
    my @steps;
    while ( $here ne $start ) {
        my $from = $leads_to->{ "$here" } or last;

        my $delta = $here - $from->coord;

        my $direction = 'IN_';
        $direction .= $delta->y ? ( $delta->y >= 0 ) ? 'S' : 'N' : '';
        $direction .= $delta->x ? ( $delta->x >= 0 ) ? 'E' : 'W' : '';

        my $xy = $here - $view;

        $SPRITES{$direction}->draw_xy(
            $show->app,
            $xy->x * $tile_width  * $scale,
            $xy->y * $tile_height * $scale,
        );

        if ($prev) {
            my $delta = $here - $prev;

            my $direction = 'OUT_';
            $direction .= $delta->y ? ( $delta->y >= 0 ) ? 'N' : 'S' : '';
            $direction .= $delta->x ? ( $delta->x >= 0 ) ? 'W' : 'E' : '';

            $SPRITES{$direction}->draw_xy(
                $show->app,
                $xy->x * $tile_width  * $scale,
                $xy->y * $tile_height * $scale,
            );
        }

        $prev = $here;
        $here = $from->coord;
    }
}

sub redraw {
    my ( $self, $stage ) = @_;

    my $leads_to = $self->path_map or return;

    my $start = $self->start;
    my $here  = $self->end or return;

    while ( $here ne $start ) {
        my $from = $leads_to->{ $here } or last;
        $from = $from->coord;

        $stage->redraw($from);
        $here = $from;
    }
}

sub to_path {
    my ( $self, $grid ) = @_;

    my $leads_to = $self->path_map or return;

    my $start = $self->start;
    my $end   = $self->end or return;

    my $this_node = $grid->get_cell( $end ) or return;

    my @nodes;
    while ( $this_node->id ne $start ) {
        my $next_node = $leads_to->{ $this_node->id } or last;
        push @nodes, $this_node;
        $this_node = $next_node;
    }

    return unless @nodes;

    my $path = Graph::Base::Path->new(
        start => $grid->get_cell( $start ),
    );

    $path->add($_) for reverse @nodes;

    return $path;
}

1;
