package Local::UI::Box;

use Moo;
use Log::Any '$log';

extends 'Popsicle::UI::HBox';

with qw(
    Popsicle::Component::UI::Area
    Popsicle::Role::Handles::Show
    Popsicle::Role::Emitter
);

has color => (
    is      => 'ro',
    default => sub { [ rand(256), rand(256), rand(256), 1 ] },
);

sub on_show {
    my ( $self, $event ) = @_;
    my $stage = $event->stage;

    my $tiles = [ $stage->tile_width, $stage->tile_height ];

    $event->app->draw_rect(
        [
            @{ ( $self->position   * $tiles )->round },
            @{ ( $self->dimensions * $tiles )->round },
        ],
        $self->color,
    );
}

sub on_enter { shift->forward( enter => shift ) }
sub on_leave { shift->forward( leave => shift ) }
sub on_click { shift->forward( click => shift ) }

1;
