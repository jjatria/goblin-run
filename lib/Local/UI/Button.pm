package Local::UI::Button;

use Moo;
use Types::Standard 'Str';
extends 'Popsicle::UI::HBox';

use SDLx::Text;
use SDLx::Surface;

with qw(
    Popsicle::Role::Handles::Show
    Popsicle::Component::UI::Area
    Popsicle::Role::Emitter
);

has text => (
    is => 'ro',  # This probably will have to be rw in the future
    isa => Str,
    default => '',
);

has surface => (
    is => 'ro',
    lazy => 1,
    default => sub {
        my $self = shift;
        SDLx::Surface->new(
            width  => $self->width,
            height => $self->height,
        );
    },
);

sub on_show {
    my ( $self, $show ) = @_;

    $self->surface->draw_rect( $self->rectangle(1), 0xFFFFFFFF );

    my $centre = $self->centre(1);

    SDLx::Text->new(
        h_align => 'center',
        color => 0x000000FF,
        x => $centre->x,
        y => $centre->y,
    )->write_to(
        $self->surface,
        $self->text,
    );

    $self->surface->blit(
        $show->app,
        undef,
        $self->rectangle,
    );
}

sub on_click { shift->forward( click => shift ) }

1;
