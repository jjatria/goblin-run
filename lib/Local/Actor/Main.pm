package Local::Actor::Main;

use Moo;

use Carp qw( croak );
use Local::Helpers qw( load_sprite );
use Log::Any qw( $log );
use Popsicle::Types qw( Vector Stage );

with qw(
    Local::Role::Actor
    Local::Role::Selectable
    Popsicle::Role::Emitter
    Popsicle::Component::UI::Area
);

use namespace::clean;

has '+walking_speed' => ( default => 0.15 );

has '+position'   => default => sub { [ 5, 5 ] };
has '+dimensions' => default => sub { [ 1, 1 ] };

has origin => ( is => 'rw', predicate => 1, clearer => 1, isa => Vector );

has 'sheet' => ( is => 'ro', default => 'assets/sample.png' );

has 'breadcrumbs' => ( is => 'rw', clearer => 1 );

has 'selector' => (
    is        => 'rw',
    isa       => Stage,
    clearer   => 1,
    predicate => 'is_selected',
);

has '+sprite' => (
    lazy => 1,
    default => sub {
        load_sprite(
            image     => $_[0]->sheet,
            animated  => 1,
            autostart => 0,
            ticks_per_frame => 5,
            clip => [ 0, 0, 32, 32 ],
            sequence  => 'idle_s',
            sequences => {
                idle_s  => [ [ 0, 0 ] ],
                idle_se => [ [ 0, 1 ] ],
                idle_e  => [ [ 0, 2 ] ],
                idle_ne => [ [ 0, 3 ] ],
                idle_n  => [ [ 0, 4 ] ],
                idle_nw => [ [ 0, 5 ] ],
                idle_w  => [ [ 0, 6 ] ],
                idle_sw => [ [ 0, 7 ] ],
                walk_s  => [ [ 1, 0 ], [ 2, 0 ], [ 3, 0 ], [ 4, 0 ] ],
                walk_se => [ [ 1, 1 ], [ 2, 1 ], [ 3, 1 ], [ 4, 1 ] ],
                walk_e  => [ [ 1, 2 ], [ 2, 2 ], [ 3, 2 ], [ 4, 2 ] ],
                walk_ne => [ [ 1, 3 ], [ 2, 3 ], [ 3, 3 ], [ 4, 3 ] ],
                walk_n  => [ [ 1, 4 ], [ 2, 4 ], [ 3, 4 ], [ 4, 4 ] ],
                walk_nw => [ [ 1, 5 ], [ 2, 5 ], [ 3, 5 ], [ 4, 5 ] ],
                walk_w  => [ [ 1, 6 ], [ 2, 6 ], [ 3, 6 ], [ 4, 6 ] ],
                walk_sw => [ [ 1, 7 ], [ 2, 7 ], [ 3, 7 ], [ 4, 7 ] ],
            },
        ),
    }
);

sub on_click {
    my ( $self, $event ) = @_;

    $log->tracef('Click on actor');

    use SDL::Events ':button';
    return unless $event->button == SDL_BUTTON_LEFT;

    $event->stop;

    # Left click
    $event->stage->select_entity($self);
}

sub on_select {
    my ( $self, $stage ) = @_;
    $log->debugf( '%s is now selected', ref($self) );

    my $breadcrumbs = $self->breadcrumbs;

    unless ($breadcrumbs) {
        my $start = $stage->map->get_cell( $self->position )
            or croak 'Start point is not on grid?';

        use Graph::Base::PathFinder;
        my $scout = Graph::Base::PathFinder->new( graph => $stage->map->grid );
        my $leads_to = $scout->dijkstra(
            start    => $start,
            max_cost => 10,
        );

        use Local::BreadCrumbs;
        $breadcrumbs = Local::BreadCrumbs->new(
            start    => $self->position,
            path_map => $leads_to,
        );

        $self->breadcrumbs( $breadcrumbs );
    }

    $self->selector( $stage );

    $stage->add_entity( $breadcrumbs );
}

sub on_deselect {
    my ( $self, $stage ) = @_;

    if ( my $breadcrumbs = $self->breadcrumbs ) {
        $breadcrumbs->redraw($stage);
        $breadcrumbs->clear_end;
        $stage->remove_entity( $breadcrumbs );
    }

    $self->clear_selector;

    $log->debugf( '%s is no longer selected', ref($self) );
}

sub on_move {
    my ( $self, $move ) = @_;

    my $stage = $move->stage;
    my $step  = $move->step;

    ###

    $self->walk( $stage, $step, $move->app, $move->dt ) if $self->has_path;

    ###

    my $sequence;
    my $prev = $self->sprite->sequence;

    if ( $self->dy || $self->dx ) {
        $sequence = 'walk_';
        $sequence .= $self->dy ? ( $self->dy >= 0 ) ? 's' : 'n' : '';
        $sequence .= $self->dx ? ( $self->dx >= 0 ) ? 'e' : 'w' : '';
    }
    else {
        $sequence = $prev;
        $sequence =~ s/walk_/idle_/;
    }

    if ($sequence) {
        if ( $self->sprite->sequence ne $sequence ) {
            $self->sprite->sequence($sequence);
            $self->sprite->start;
        }
    }
    else {
        $self->sprite->reset;
    }

    return unless $self->dy || $self->dx;

    $stage->redraw($_)
        for $stage->map->grid->neighbourhood( $self->position->round );

    $self->move;

    $self->x = 0 if $self->x < 0;
    $self->y = 0 if $self->y < 0;

    $self->x = $stage->tiles_wide - 1 if $self->x >= $stage->tiles_wide;
    $self->y = $stage->tiles_high - 1 if $self->y >= $stage->tiles_high;
};

sub on_show {
    my ( $self, $show ) = @_;
    $self->draw( $show->stage, $show->app );
};

sub walk {
    my ( $self, $stage, $step, $app, $time ) = @_;

    my $path = $self->path
        or die 'Cannot walk if creature has no path';

    # `origin` stores where this creature is currently walking from
    # If there is no origin, we set it to the start of the path

    unless ( $self->has_origin ) {
        # If there is no path, then we are ready to stop
        if ( $path->length ) {
            # Set origin and creature position to current start of path
            my $origin = $path->step->coord;
            $self->position( $origin );
            $self->origin( $origin ) or die 'No origin set!';
        }
        else {
            # If origin has been cleared and path has no length
            # the creature has arrived at destination

            # Snap to grid
            $self->clear_path;
            $self->position( $self->position->round );

            # If we are selected, re-select so we can get the marker
            # on to the new position
            $self->selector->select_entity($self)
                if $self->is_selected;
        }
    }

    # If we still have no origin, we've finished
    unless ( $self->has_origin ) {
        $self->stop_moving;
        $stage->redraw($self->position);
        return;
    }

    # Set creature motion
    my $v = $path->start->coord - $self->origin;
    $self->velocity( $v->normalise * $self->walking_speed );

    # Have we moved to the next tile?
    # FIXME This is not very smooth, and makes movement jerky
    my $distance = $self->origin - $self->position;
    if ( abs( $distance->x ) + $self->walking_speed > 1 || abs( $distance->y ) + $self->walking_speed > 1 ) {
        $self->emit('tile');
        $self->clear_origin;
    }
}

1;
