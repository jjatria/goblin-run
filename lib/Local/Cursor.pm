package Local::Cursor;

use Moo;
use Types::Standard qw( Bool );
use Local::Helpers qw( load_sprite );

with qw(
    Local::Role::Sprite
    Popsicle::Role::Handles::Event
);

use namespace::clean;

has active => ( is => 'rw', isa => Bool, default => 0 );

after active => sub {
    $_[0]->sprite->sequence( $_[0]->{active} ? 'active' : 'inactive' )
};

has '+sprite' => (
    lazy => 1,
    default => sub {
        load_sprite(
            image     => 'assets/cursor.png',
            animated  => 1,
            clip => [ 0, 0, 32, 32 ],
            sequence  => 'inactive',
            sequences => {
                inactive => [ [ 0, 0 ] ],
                active   => [ [ 1, 0 ] ],
            },
        ),
    }
);

has '+position_is_relative' => ( default => 0 );

sub on_show {
    my ( $self, $show ) = @_;
    $self->draw( $show->stage, $show->app );
};

sub on_event  {
    my ( $self, $event ) = @_;

    if ( $event->isa('Popsicle::Event::Mouse::Released') ) {
        $self->forward( click => $event ) if $self->active;

        $self->active(0);
        $self->redraw( $event->stage );
    }
    elsif ( $event->isa('Popsicle::Event::Mouse::Pressed') ) {
        $self->active(1);
        $self->redraw( $event->stage );
    }
};

1;
