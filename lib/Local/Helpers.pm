package Local::Helpers;

use Class::Load qw( load_class );
use Ref::Util qw( is_arrayref );
use Exporter::Shiny qw( scale_sprite load_sprite );

use namespace::clean;

sub load_sprite {
    my %args = @_;

    if ( is_arrayref $args{clip} ) {
        require SDLx::Rect;
        my ( $x, $y, $w, $h ) = @{ $args{clip} };
        $args{clip} = SDLx::Rect->new( $x, $y, $w, $h  );
    }

    my $class = 'SDLx::Sprite';
    $class .= '::Animated' if delete $args{animated};
    my $start = 1 if $class =~ /animated/i and delete $args{autostart};

    my $scale = delete $args{scale} // 1;

    load_class $class;
    my $sprite = $class->new(%args);

    scale_sprite( $sprite, 0, $scale ) if $scale > 1;

    $sprite->start if $start;

    return $sprite;
}

sub scale_sprite {
    my ( $self, $angle, $zoom ) = @_;

    my $work;
    $work = 1 if $angle != 0 || $zoom != 1;

    if ( $work && $self->{orig_surface} ) {

        require SDL;
        require SDLx::Surface;
        require SDL::GFX::Rotozoom;

        my $scaled = SDL::GFX::Rotozoom::surface(
                $self->{orig_surface}, #prevents scaling of the surface
                $angle // 0,
                $zoom // 1,
                0,
        ) or Carp::confess 'scaling error: ' . SDL::get_error;

        #After scaling the surface is on a undefined background.
        #This causes problems with alpha. So we create a surface with a fill of the src_color.
        #This insures less artifacts.
        if ( $self->{alpha_key} ) {
                my $background = SDLx::Surface::duplicate( $scaled );
                $background->draw_rect(
                        [ 0, 0, $background->w, $background->h ],
                        $self->{alpha_key}
                );
                SDLx::Surface->new( surface => $scaled )->blit($background);

                $self->handle_surface( $background->surface );
                $self->alpha_key( $self->{alpha_key} );
        } else {
                $self->handle_surface($scaled);
        }

        $self->alpha( $self->{alpha} ) if $self->{alpha};
        $self->{angle} = $angle;
        $self->{zoom} = $zoom;
    }

    return $self;
}

1;
