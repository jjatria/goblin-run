package Popsicle::Helpers;

use strict;
use warnings;

use Exporter::Shiny qw( map_from_tmx );

use namespace::clean;

sub map_from_tmx {
    my ( %params ) = @_;

    require Popsicle::Map;
    require Games::TMX::Parser;

    my $tmx = Games::TMX::Parser->new(
        map_dir  => $params{root},
        map_file => $params{filename},
    )->map;

    my $map = Popsicle::Map->new(
        dimensions  => [ $tmx->width, $tmx->height ],
        tilesets    => $tmx->tilesets,
        tile_width  => $tmx->tile_width,
        tile_height => $tmx->tile_height,
    );

    for my $layer ( @{ $tmx->ordered_layers } ) {
        require Popsicle::Map::Grid;

        my $grid = Popsicle::Map::Grid->new(
            dimensions => [ $tmx->width, $tmx->height ],
            lazy       => 1,
        );

        for my $cell ( $layer->all_cells ) {
            my ( $x, $y ) = $cell->xy;

            my %props;

            my $tile  = $cell->tile;
            %props = %{ $tile->properties } if $tile;

            $grid->add_cell(
                [ $x, $y ],
                $props{impassable} ? ( passable => 0 )     : (),
                $tile              ? ( tile     => $tile ) : (),
            );
        }

        $grid->lock;

        push @{ $map->layers }, $grid;
    }

    # Connect cells in bottom-most layer of map
    for my $grid ( $map->grid ) {
        for my $from_node ( $grid->nodes ) {
            next unless $from_node->is_passable;
            my $from = $from_node->coord;

            for ( $grid->neighbourhood($from) ) {
                next if $_ eq $from;
                next unless $grid->in_bounds($_);

                my $to_node = $grid->get_cell($_) or next;

                next unless $to_node->is_passable;

                my $to    = $to_node->coord;
                my $delta = $to - $from;

                my %params;

                # Is this a diagonal?
                if ( $delta->length > 1 ) {
                    my $a = $grid->get_cell( $from + [ $delta->x, 0 ] )
                        or next;

                    next unless $a->is_passable;

                    my $b = $grid->get_cell( $from + [ 0, $delta->y ] )
                        or next;

                    next unless $b->is_passable;

                    # Diagonal cost
                    $params{weight} = 1.5;
                }

                $grid->add_edge( $from_node, $to_node, %params );
            }
        }
    }

    return $map;
}

1;
