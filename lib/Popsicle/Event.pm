package Popsicle::Event;

use Moo;
extends 'Beam::Event';

has stage => ( is => 'rw', weak_ref => 1 );

1;
