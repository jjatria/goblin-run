package Popsicle::Algorithm::LOS;

use Moo::Role;

requires 'compute';
requires 'symmetric';

has blocks_light => ( is => 'ro', required => 1 );
has get_distance => ( is => 'ro', required => 1 );
has set_visible  => ( is => 'ro', required => 1 );

1;

=pod

A role for a component implementing a visibility algorithm

=cut
