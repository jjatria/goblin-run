package Popsicle::Algorithm::LOS::BeveledCells;

use Moo;
use Popsicle::Slope;
use Log::Any '$log';

use namespace::clean;

has symmetric => ( is => 'ro', default => 0 );

with qw( Popsicle::Algorithm::LOS );

sub compute {
    my ( $self, $origin, $range ) = @_;

    $self->set_visible->( Popsicle::Vector->new($origin) );

    # Run for each octant
    for my $octant ( 0 .. 7 ) {
        $self->_do_compute_visibility(
            $octant, $origin, $range, 1, [ 1, 1 ], [ 1, 0 ] )
    }
}

sub _do_compute_visibility {
    my $self =  shift;
    my ( $octant, $origin, $range, $first_column, $start, $end ) = @_;

    $origin = Popsicle::Vector->new($origin);
    $start    = Popsicle::Slope->new($start);
    $end = Popsicle::Slope->new($end);

    $log->debugf( 'Computing for octant %d', $octant );

    $log->tracef('%s', {
        octant => $octant,
        origin => "[$origin]",
        range  => $range,
        first_column      => $first_column,
        start    => "[$start] (" . $start->value . ")",
        end => "[$end] (" . $end->value . ")",
    });

    # NOTE: the code duplication between blocks_light and set_visible is
    # for performance. Don't refactor the octant translation out unless
    # you don't mind an 18% drop in speed

    my $blocks_light = $self->blocks_light;
    my $blocks_light_wrapper = sub {
        my ( $coord, $octant, $origin ) = @_;

        my $new = Popsicle::Vector->new($origin);
        $coord = Popsicle::Vector->new($coord);

        $origin = Popsicle::Vector->new($origin);

        $new
            += $octant == 0 ? [  $coord->[0], -$coord->[1] ]
            :  $octant == 1 ? [  $coord->[1], -$coord->[0] ]
            :  $octant == 2 ? [ -$coord->[1], -$coord->[0] ]
            :  $octant == 3 ? [ -$coord->[0], -$coord->[1] ]
            :  $octant == 4 ? [ -$coord->[0],  $coord->[1] ]
            :  $octant == 5 ? [ -$coord->[1],  $coord->[0] ]
            :  $octant == 6 ? [  $coord->[1],  $coord->[0] ]
            :  $octant == 7 ? [  $coord->[0],  $coord->[1] ]
            : die 'Not a supported octant';

        return $blocks_light->( $new, $origin );
    };

    my $set_visible = $self->set_visible;
    my $set_visible_wrapper = sub {
        my ( $coord, $octant, $origin ) = @_;
        my $new = Popsicle::Vector->new($origin);
        $coord = Popsicle::Vector->new($coord);

        $new
            += $octant == 0 ? [  $coord->[0], -$coord->[1] ]
            :  $octant == 1 ? [  $coord->[1], -$coord->[0] ]
            :  $octant == 2 ? [ -$coord->[1], -$coord->[0] ]
            :  $octant == 3 ? [ -$coord->[0], -$coord->[1] ]
            :  $octant == 4 ? [ -$coord->[0],  $coord->[1] ]
            :  $octant == 5 ? [ -$coord->[1],  $coord->[0] ]
            :  $octant == 6 ? [  $coord->[1],  $coord->[0] ]
            :  $octant == 7 ? [  $coord->[0],  $coord->[1] ]
            : die 'Not a supported octant';

        return $set_visible->($new);

    };

    # Throughout this function there are references to various parts of tiles.
    # A tile's coordinates refer to its center, and the following diagram shows
    # the parts of the tile and the vectors from the origin that pass through
    # those parts.
    #
    # Given a part of a tile with vector u, a vector v passes above it if v > u
    # and below it if v < u.
    #
    #     g         center:               y       /   x       =  3 /  7 = 0.429
    #  a------b   a top left:           ( y*2+1 ) / ( x*2-1 ) =  7 / 13 = 0.539
    #  |  /\  |   b top right:          ( y*2+1 ) / ( x*2+1 ) =  7 / 15 = 0.466
    #  |i/__\j|   c bottom left:        ( y*2-1 ) / ( x*2-1 ) =  5 / 13 = 0.385
    # e|/|  |\|f  d bottom right:       ( y*2-1 ) / ( x*2+1 ) =  5 / 15 = 0.333
    #  |\|__|/|   e middle left:        ( y*2 )   / ( x*2-1 ) =  6 / 13 = 0.462
    #  |k\  /m|   f middle right:       ( y*2 )   / ( x*2+1 ) =  6 / 15 = 0.4
    #  |  \/  |   g top center:         ( y*2+1 ) / ( x*2 )   =  7 / 14 = 0.5
    #  c------d   h bottom center:      ( y*2-1 ) / ( x*2 )   =  5 / 14 = 0.357
    #     h       i inner top left:     ( y*4+1 ) / ( x*4-1 ) = 13 / 27 = 0.482
    #             j inner top right:    ( y*4+1 ) / ( x*4+1 ) = 13 / 29 = 0.448
    #             k inner bottom left:  ( y*4-1 ) / ( x*4-1 ) = 11 / 27 = 0.407
    #             m inner bottom right: ( y*4-1 ) / ( x*4+1 ) = 11 / 29 = 0.379
    #
    # a-d are the corners of the tile
    # e-h are the corners of the inner (wall) diamond
    # i-m are the corners of the inner square (1/2 tile width)
    #
    # Slope is 0 when it is horizontal, 1 when it gets to the end of the octant
    #
    #
    #       -->   <--
    #     \  3  | 2   /
    #  |    \   |   /    |
    #  v  4   \ | /   1  v
    #     - - - x - - -
    #  ^  5   / | \   8  ^
    #  |    /   |   \    |
    #     /  6  |  7  \
    #       -->   <--
    #

    for my $x ( $first_column .. $range ) {

        # Compute the Y coordinates of the top and bottom of the sector.
        # We maintain that start > end

        my $start_y;

        # If $start == [ 1, ? ] then it must be [ 1, 1 ]
        # because [ 1, 0 ] < $start <= [ 1, 1 ].
        #
        # This is special-cased because $start starts at [ 1, 1 ] and
        # remains [ 1, 1 ] as long as it doesn't hit anything, so it's a
        # common case

        if ( $start->x == 1) {
            $start_y = $x;
        }
        else { # $start < 1
            # Get the tile that the start vector enters from the left. Since our
            # coordinates refer to the center of the tile, this is
            #
            #     ( $x - 0.5 ) * $start + 0.5
            #
            # which can be computed as
            #
            #     ( $x - 0.5 ) * $start + 0.5
            #     = ( 2 * ( $x + 0.5 ) * $start + 1 ) / 2
            #     = ( ( 2 * $x + 1 ) * $start + 1 ) / 2
            #
            # since $start == [ a, b ], this is
            #
            #     ( ( 2 * $x + 1 ) * a + b ) / 2 * b
            #
            # If it enters a tile at one of the left corners, it will round up,
            # so it'll enter from the bottom-left and never the top-left

            # The Y coordinate of the tile entered from the left
            $start_y = ( ( $x * 2 - 1 ) * $start->y + $start->x ) / ( $start->x * 2 );

            # Now it's possible that the vector passes from the left side of
            # the tile up into the tile above before exiting from the right
            # side of this column. So we may need to increment $start_y.

            # If the tile blocks light (i.e. is a wall)...
            if ( $blocks_light_wrapper->( [ $x, $start_y ], $octant, $origin ) ) {
                # If the tile entered from the left blocks light, whether
                # it passes into the tile above depends on the shape of the
                # wall tile as well as the angle of the vector.
                #
                # If the tile has does not have a beveled top-left corner,
                # then it is blocked. The corner is beveled if the tiles above
                # and to the left are not walls. We can ignore the tile to the
                # left because if it was a wall tile, the start vector must have
                # entered this tile from the bottom-left corner, in which case
                # it can't possibly enter the tile above.
                #
                # Otherwise, with a beveled top-left corner, the slope of the
                # vector must be greater than or equal to the slope of the
                # vector to the top center of the tile
                #
                #     ( $x * 2, $start_y * 2 + 1 )
                #
                # in order for it to miss the wall and pass into the tile above

                $start_y++
                    if $start >= [ $x * 2, $start_y * 2 + 1 ]
                    && ! $blocks_light_wrapper->(
                        [ $x, $start_y + 1 ], $octant, $origin
                    );
            }
            else { # The tile doesn't block light
                # Since this tile doesn't block light, there's nothing to stop
                # it from passing into the tile above, and it does so if the
                # vector is greater than the vector for the bottom-right corner
                # of the tile above.
                #
                # However, there is one additional consideration.
                #
                # Later code in this method assumes that if a tile blocks light
                # then it must be visible, so if the tile above blocks light we
                # have to make sure the light actually impacts the wall shape.
                #
                # Now there are three cases:
                #
                # 1. The tile above is clear, in which case the vector must be
                # above the bottom-right corner of the tile above;
                #
                # 2. The tile above blocks light and does not have a beveled
                # bottom-right corner, in which case the vector must be above
                # the bottom-right corner; and
                #
                # 3. The tile above blocks light and does have a beveled
                # bottom-right corner, in which case the vector must be above
                # the bottom center of the tile above (i.e. the corner of the
                # beveled edge).
                #
                # It's possible to merge 1 and 2 into a single check, and we
                # get the following:
                #
                # If the tile above and to the right is a wall, then the vector
                # must be above the bottom-right corner. Otherwise, the vector
                # must be above the bottom center. This works because if the
                # tile above and to the right is a wall, then there are two
                # cases:
                #
                # 1. The tile above is also a wall, in which case we must
                # check against the bottom-right corner; or
                #
                # 2. The tile above is not a wall, in which case the vector
                # passes into it if it's above the bottom-right corner.
                #
                # So either way we use the bottom-right corner in that case.
                #
                # Now, if the tile above and to the right is not a wall, then
                # we again have two cases:
                #
                # 1. The tile above is a wall with a beveled edge, in which
                # case we must check against the bottom center; or
                #
                # 2. The tile above is not a wall, in which case it will only
                # be visible if light passes through the inner square, and the
                # inner square is guaranteed to be no larger than a wall
                # diamond, so if it wouldn't pass through a wall diamond then
                # it can't be visible, so there's no point in incrementing
                # $start_y even if light passes through the corner of the tile
                # above. so we might as well use the bottom center for both
                # cases.

                my $ax = $x * 2; # center

                # Use bottom-right if the tile above and right is a wall
                $ax++
                    if $blocks_light_wrapper->( [ $x + 1, $start_y + 1 ], $octant, $origin );

                $start_y++ if $start > [ $ax, $start_y * 2 + 1];
            }
        }

        my $end_y;

        # If $end == [ ?, 0 ], then it's hitting the tile at Y = 0 dead
        # center. This is special-cased because $end->y starts at zero
        # and remains zero as long as it doesn't hit anything,
        # so it's common.

        if ( $end->y == 0 ) {
            $end_y = 0;
        }
        else { # end > 0
            # The tile that the end vector enters from the left
            $end_y
                = int( ( ( $x * 2 - 1 ) * $end->y + $end->x ) / ( $end->x * 2) );

            # Code below assumes that if a tile is a wall then it's
            # visible, so if the tile contains a wall we have to ensure
            # that the end vector actually hits the wall shape. It
            # misses the wall shape if the top-left corner is beveled and
            #
            #     $end >= ( $end_y * 2 + 1 ) / ( $x * 2 )
            #
            # Finally, the top-left corner is beveled if the tiles to the
            # left and above are clear. We can assume the tile to the left
            # is clear because otherwise the end vector would be
            # greater, so we only have to check above

            $end_y++
                if $end >= [ $x * 2, $end_y * 2 + 1 ]
                && $blocks_light_wrapper->( [ $x, $end_y ], $octant, $origin )
                && ! $blocks_light_wrapper->( [ $x, $end_y + 1 ], $octant, $origin );
        }

        # Go through the tiles in the column now that we know which ones
        # could possibly be visible

        my $was_opaque; # 0: false, 1: true, undef: not applicable

        $log->tracef( 'The column for %s goes from %s to %s', $x, $start_y, $end_y );

        for ( 0 .. $start_y - $end_y ) {
            my $y = $start_y - $_;

            $log->tracef( 'Looking at [ %s,%s ]', $x, $y );

            # Skip the tile if it's out of visual range

            my $distance = $self->get_distance->([ $x, $y ]);
            $log->tracef( 'Tile at [ %s,%s ] is at %.3f', $x, $y, $distance );

            unless ( $range < 0 || $distance <= $range ) {
                $log->tracef(
                    'Skipiing tile at [ %s,%s ] because its out of range',
                    $x, $y
                );
                next;
            }

            $log->tracef( 'Tile at [ %s,%s ] is within range (%s)', $x, $y, $range );

            my $is_opaque = $blocks_light_wrapper->([ $x, $y ], $octant, $origin );

            $log->tracef( 'Tile is %sopaque', $is_opaque ? '' : 'NOT ' );

            # Every tile where $start_y > $y > $end_y is guaranteed to
            # be visible. Also, the code that initializes $start_y and
            # $end_y guarantees that if the tile is opaque then
            # it's visible. So we only have to do extra work for the
            # case where the tile is clear and $y == $start_y or
            # $y == $end_y.
            #
            # If $y == $start_y then we have to make sure that the start
            # vector is above the bottom-right corner of the inner
            # square.
            #
            # If $y == $end_y then we have to make sure that the
            # end vector is below the top-left corner of the inner
            # square

            my $hits_inner_square;

            if ($self->symmetric) {
                # This ensures that a clear tile is visible only if
                # there's an unobstructed line to its center.
                my $center = Popsicle::Slope->new([ $x, $y ]);

                $hits_inner_square = (
                    ( $y != $start_y || $start >= $center )
                    && ( $y != $end_y || $end <= $center )
                );
            }
            else {
                my $inner_bottom_right = Popsicle::Slope->new([ $x * 4 + 1, $y * 4 - 1 ]);
                my $inner_top_left     = Popsicle::Slope->new([ $x * 4 - 1, $y * 4 + 1 ]);

                $hits_inner_square = (
                    ( $y != $start_y || $start > $inner_bottom_right )
                    && ( $y != $end_y || $end < $inner_top_left )
                );

            }

            my $is_visible = $is_opaque || $hits_inner_square;

            $set_visible_wrapper->([ $x, $y], $octant, $origin )
                if $is_visible;

            # If we found a transition from clear to opaque or
            # vice-versa, adjust the start and end vectors, but don't
            # bother adjusting them if this is the last column anyway
            if ( $x != $range ) {
                $log->tracef( 'We are not at the last column' );

                if ($is_opaque) {
                    $log->tracef( 'The tile we hit is opaque' );

                    # If we found a transition from clear to opaque,
                    # this sector is done in this column, so adjust
                    # the end vector upward and continue processing
                    # it in the next column
                    if (defined $was_opaque && !$was_opaque) {
                        $log->tracef( 'And we hit a transition from transparent to opaque' );

                        # If the opaque tile has a beveled top-left
                        # corner, move the end vector up to the top
                        # center. Otherwise, move it up to the top
                        # left.
                        #
                        # The corner is beveled if the tiles above and
                        # to the left are clear.
                        #
                        # We can assume the tile to the left is clear
                        # because otherwise the vector would be higher,
                        # so we only have to check the tile above.

                        # top center by default
                        my $new_end = Popsicle::Slope->new([ $x * 2, $y * 2 + 1 ]);

                        # Top left if the corner is not beveled
                        # (only if algorithm is not symmetric)
                        $new_end->[0]--
                            if ! $self->symmetric
                            && $blocks_light_wrapper->( [ $x, $y + 1 ], $octant, $origin);

                        # We have to maintain the invariant that
                        # $start > $end, so the new sector created
                        # by adjusting the end is only valid if
                        # that's the case

                        if ( $start > $new_end ) {
                            $log->tracef( 'Start slope is still greater than new end' );

                            # If we're at the end of the column,
                            # then just adjust the current sector
                            # rather than recursing since there's no
                            # chance that this sector can be split in
                            # two by a later transition back to clear

                            # Don't recurse unless necessary
                            if ( $y == $end_y ) {
                                $log->tracef(
                                    'We are already at the end of the column because %s == %s',
                                    $y, $end_y
                                );

                                $end = $new_end;
                                last;
                            }
                            else {
                                $log->tracef( 'Calling function recursively' );
                                $self->_do_compute_visibility(
                                    $octant,
                                    $origin,
                                    $range,
                                    $x + 1,
                                    $start,
                                    $new_end,
                                );
                                $log->tracef('Back from recursive function call');
                            }
                        }
                        # The new end is greater than or equal to the
                        # top, so the new sector is empty and we'll ignore
                        # it.
                        #
                        # If we're at the end of the column, we'd
                        # normally adjust the current sector rather than
                        # recursing, so that invalidates the current
                        # sector and we're done
                        elsif ( $y == $end_y ) {
                            $log->tracef('The new end is <= the start, and we are at the end. Done!' );
                            return ;
                        }
                        else {
                            $log->fatalf( 'What does this mean?' );
                        }
                    }

                    $log->tracef('Remembering tile WAS opaque');
                    $was_opaque = 1;
                }
                else {
                    $log->tracef('Tile is not opaque');

                    # If we found a transition from opaque to clear,
                    # adjust the start vector downwards
                    if ( $was_opaque ) {
                        $log->tracef('But it was!');

                        # If the opaque tile has a beveled bottom-right
                        # corner, move the start vector down to the bottom
                        # center.
                        #
                        # Otherwise, move it down to the bottom right.
                        # The corner is beveled if the tiles below and to
                        # the right are clear.
                        #
                        # We know the tile below is clear because that's
                        # the current tile, so just check to the right

                        # The bottom of the opaque tile ( $oy * 2 - 1 )
                        # equals the top of this tile ( $y * 2 + 1 )

                        my $new_start = Popsicle::Slope->new([ $x * 2, $y * 2 + 1 ]);

                        # NOTE: if you're using full symmetry and want
                        # more expansive walls (recommended), comment out
                        # the next line

                        # Check the right of the opaque tile ( $y + 1 ),
                        # not this one
                        $new_start->[0]++ if $blocks_light_wrapper->(
                            [ $x + 1, $y + 1 ], $octant, $origin );

                        # We have to maintain the invariant that
                        # $start > $end. if not, the sector is empty
                        # and we're done
                        return if $end >= $new_start;
                        $start = $new_start;
                    }

                    $log->tracef('Remembering tile was NOT opaque');
                    $was_opaque = 0;
                }
            }
        }

        # If the column didn't end in a clear tile, then there's no
        # reason to continue processing the current sector because that
        # means either
        #
        # 1. $was_opaque == -1, implying that the sector is empty or at
        # its range limit; or
        #
        # 2. $was_opaque == 1, implying that we found a transition from
        # clear to opaque and we recursed and we never found a
        # transition back to clear, so there's nothing else for us to
        # do that the recursive method hasn't already.
        #
        # If we didn't recurse (because $y == $end_y), it would
        # have executed a break, leaving $was_opaque equal to 0.
        unless ( defined $was_opaque && ! $was_opaque ) {
            $log->tracef('Tile opaqueness is undefined');
            last;
        }
    }
}

1;
