package Popsicle::Game;

use Moo;
use Carp 'croak';
with qw( Popsicle::Role::Dimensions );

use curry;
use JSON::MaybeXS qw( decode_json);
use Ref::Util qw( is_hashref );
use Path::Tiny qw( path );
use Popsicle::Types qw( Stage );
use Scalar::Util qw( blessed );
use Class::Load qw( load_class );

use namespace::clean;

has namespace => ( is => 'ro', required => 1 );

has config => ( is => 'ro', default   => sub { +{} } );

has stage => (
    is        => 'rw',
    isa       => Stage,
    predicate => 1,
    init_arg  => undef,
);

has '+dimensions' => ( default => sub { [ 320, 240 ] } );

has app => (
    is => 'ro',
    lazy => 1,
    default => sub {
        require SDLx::App;
        use SDL::Video ':video';
        SDLx::App->new(
            width  => $_[0]->width,
            height => $_[0]->height,
            title  => $_[0]->config->{core}{name},
            delay  => $_[0]->config->{app}{delay},
            flags  => SDL_RESIZABLE | SDL_ANYFORMAT,
            eoq    => 1,
        );
    },
    handles => [qw( run )],
);

has dispatcher => (
    is => 'ro',
    lazy => 1,
    default => sub {
        require Popsicle::Event::Dispatcher::SDL;
        Popsicle::Event::Dispatcher::SDL->new(
            app => shift->app,
        );
    },
);

# Read config and set initial stage if none has been defined
around BUILDARGS => sub {
    my $orig = shift;
    my $self = shift;

    my $args = $self->$orig(@_);

    $args->{config} //= {};

    $args->{config} = decode_json path( $args->{config} )->slurp
        unless is_hashref $args->{config};

    return $args;
};

# On construction, forward SDL events to stage listeners
sub BUILD {
    my ( $self, $args ) = @_;

    # Initialise app
    $self->app;

    # Load the initial stage
    $self->load_stage( delete $args->{stage} );
}

sub load_stage {
    my ( $self, $stage ) = @_;

    croak 'Cannot load undefined Stage' unless $stage;

    $stage = join( '::', $self->namespace, 'Stage', $stage )
        unless $stage =~ /^\+/;

    my $code   = $stage =~ s/.*::Stage:://r;
    my $config = $self->config->{stages}{$code};

    load_class $stage;
    $stage = $stage->new(
        game => $self,
        $config ? ( config => $config ) : (),
        @_,
    );

    my $dispatcher = $self->dispatcher;

    ( delete $self->{stage_unsubscriber} )->() if $self->{stage_unsubscriber};

    my @unsub = (
        $dispatcher->subscribe( event => $stage->curry::weak::on_event ),
        $dispatcher->subscribe( move  => $stage->curry::weak::on_move ),
        $dispatcher->subscribe( show  => $stage->curry::weak::on_show ),
    );

    $self->{stage_unsubscriber} = sub { $_->() for @unsub };

    return $self->stage($stage);
};

1;
