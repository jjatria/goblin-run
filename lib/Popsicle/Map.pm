package Popsicle::Map;

use Moo;

use Types::Standard qw( Int );

with qw( Popsicle::Role::Dimensions );

use namespace::clean;

has layers => ( is => 'ro', default => sub { [] } );

has tilesets => ( is => 'ro', default => sub { [] } );

has tile_height => ( is => 'ro', isa => Int );

has tile_width  => ( is => 'ro', isa => Int );

# The base grid of the map: the bottom-most layer
has grid => (
    is      => 'ro',
    lazy    => 1,
    default => sub { shift->layers->[0] },
);

sub get_cell {
    my ( $self, $coord ) = @_;

    my $cell;

    for my $layer ( @{ $self->layers } ) {
        $cell = $layer->get_cell($coord);
        last if $cell;
    }

    return $cell;
}

sub get_cell_stack {
    my ( $self, $coord ) = @_;

    my @stack;

    for my $layer ( @{ $self->layers } ) {
        push @stack, $layer->get_cell($coord);
        last unless wantarray;
    }

    return @stack;
}

1;

=pod

A map is a collection of layers, one on top of the next.

A map must havea at least one layer.

Map layers are grids of cells.

Each cell has a position in the grid, and a reference to a tile.

Tiles are defined in tilesets, and have arbitrary properties

=cut
