package Popsicle::Vector;

use strict;
use warnings;

use Carp 'croak';
use Scalar::Util 'blessed';
use Ref::Util 'is_arrayref';
use POSIX ();

use constant SEPARATOR => ',';

use namespace::clean;

=encoding utf-8

=head1 NAME

Popsicle::Vector - A lightweight N-dimensional vector class

=head1 DESCRIPTION

A L<Popsicle::Vector> object is an array reference, with one component per
dimension. There is no explicit limit on the number of dimensions, so both
2D and 3D vectors are allowed, although operations invoolving multiple
vectors with different number of dimensions is not supported.

=head1 SYNOPSIS

    use Popsicle::Vector;

    $a = Popsicle::Vector->new(  10,  5  );  # Initialise with array
    $b = Popsicle::Vector->new([  5, 10 ]);  # Initialise with reference

    # Calculate the difference between them
    $d = $a->subtract($b);
    $d = $a - $b;

    # Operations are non-destructive

    say $d->normalise->round . " != " . $d . "\n";
    # Prints: 1,-1 != 5,-5

=head1 ACCESSORS

For convenience, L<Popsicle::Vector> objects provide named accessors to their
first three components, using respectively the keys C<x>, C<y> and C<z>. These
are lvalues, which means they can also be used in the left-hand side of an
assignment.

The user is responsible for making sure that no invalid value is assigned to
the vector, since they do no parameter validation.

=cut

sub x : lvalue { shift->[0] }
sub y : lvalue { shift->[1] }
sub z : lvalue { shift->[2] }

=head1 METHODS

=cut

use overload
    '""'  => \&to_string,
    '-'   => \&subtract,
    '+'   => \&add,
    '*'   => \&multiply,
    '/'   => \&divide,
    'abs' => \&abs,
    '<=>' => \&compare,
    'cmp' => sub { "$_[0]" cmp "$_[1]" };

sub new {
    my $class = shift;
    $class = ref $class if blessed $class;

    my @array = is_arrayref $_[0] ? @{ +shift // [] } : @_;
    croak 'Cannot create vector without components' unless @array;
    return bless [@array], $class;
}

=head2 C<plain>

    $plain_ref = $vector->plain;

Returns an unblessed copy of the vector.

=cut

sub plain { [@{ +shift }] }

=head2 C<dimensions>

    $int = $vector->dimensions;

Returns the number of components in the vector.

=cut

sub dimensions { scalar @{ +shift } }

=head2 C<to_string>

    $str = $vector->to_string;

Stringifies the vector. Used to implement operator overlloading.

=cut

sub to_string { join SEPARATOR, @{ +shift } }

=head2 C<from_string>

    $vector = Popsicle::Vector->from_string( $other_vector->to_string )

Constructs a vector from a stringified vector.

=cut

sub from_string { shift; Popsicle::Vector->new( split( SEPARATOR, shift ) ) }

=head2 C<components>

    @components = $vector->components;

Returns a list with a copy of each individual component in the vector.

=cut

sub components { @{ +shift } }

=head2 C<round>

    $round = $vector->round;

Returns a copy of the vector with all its components converted to the nearest
integer. Conversion is done using L<lround|POSIX/lround> from POSIX, such
that 0.4999... turns into 0, but 0.5 turns into 1.

=cut

sub round { Popsicle::Vector->new( map { POSIX::lround $_ } @{ +shift } ) }

=head2 C<floor>

    $floor = $vector->floor;

Returns a copy of the vector with all its components converted to the largest
integer less than or equal to themselves. Conversion is done using
L<floor|POSIX/floor> from POSIX, such that 0.5 turns into 0.

=cut

sub floor { Popsicle::Vector->new( map { POSIX::floor $_ } @{ +shift } ) }

=head2 C<ceil>

    $ceil = $vector->ceil;

Returns a copy of the vector with all its components converted to the smallest
integer greater than or equal to themselves. Conversion is done using
L<ceil|POSIX/ceil> from POSIX, such that 0.4999... turns into 1.

=cut

sub ceil { Popsicle::Vector->new( map { POSIX::ceil $_ } @{ +shift } ) }

=head2 C<length>

    $length = $vector->length;

Returns the length of a vector, calculated as the square root of the sum of
the square of each component.

=cut

sub length {
    my $x = 0;
    $x += ( $_ * $_ ) for @{ +shift };
    return sqrt $x;
}

=head2 C<normalise>

    $normal = $vector->normalise;
    $normal->length == 1 or die;

Returns a unit-length copy of the vector.

If run on a vector with no length (one whose components are all equal to zero),
it returns a copy of the vector.

=cut

sub normalise {
    my $self = shift;
    my $length = $self->length;
    return Popsicle::Vector->new( (0) x @{$self} ) unless $length;
    return $self->divide( $length );
}

=head2 C<reverse>

    $reverted = $vector->reverse;

Returns a copy of the vector in which the order of components is reversed,
such that the last one is the first and vice-versa.

=cut

sub reverse { Popsicle::Vector->new( reverse @{ +shift } ) }

=head2 C<clone>

    $clone = $vector->clone;

Returns a copy of the vector.

=cut

sub clone { Popsicle::Vector->new( @{ +shift } ) }

=head2 C<zero>

    $zero = $vector->zero;

Returns a vector similar to the original one, but with all of its components
set to zero.

=cut

sub zero { shift() * 0  }

=head2 C<compare>

    $reverted = $vector->reverse;

Returns a copy of the vector in which the order of components is reversed,
such that the last one is the first and vice-versa.

=cut

sub compare {
    my ( $self, $other, $swap ) = @_;

    croak sprintf 'Cannot compare a Vector and an undefined value'
        unless defined $other;

    my $target = blessed $other && $other->can('length')
        ? $other->length : $other;

    return $self->length <=> $target unless $swap;
    return $target <=> $self->length;
}

=head2 C<slope>

    $slope = $vector->slope;

Returns the slope of the vector. This will be an instance of L<Popsicle::Slope>.

=cut

sub slope {
    require Popsicle::Slope;
    return Popsicle::Slope->new( shift->plain );
}

=head1 ENTRYWISE OPERATIONS

All of the operations described below are entrywise operations, meaning that
they pair each component of one vector with the corresponding component of the
other.

In addition to accepting vectors as their arguments, they will accept plain
array references and plain numbers.

When acting on an array reference, this will be interpretes as if it had been
used to construct a L<Popsicle::Vector> object first.

When acting on a numeric scalar, this will be interpretes as if it were a
L<Popsicle::Vector> object with the same number of components as the other
operand, all of which have the provided value.

So, for any operator, the following are all acceptable:

    $a = Popsicle::Vector->new(...);
    $b = Popsicle::Vector->new(...);

    # All these are valid
    $c = $a->METHOD($b);
    $c = $a OP $b;
    $c = $a OP [ $b->components ];
    $c = $a OP $number;

    $c = Popsicle::Vector->new( map { $a->[$_] OP $b->[$_] } 0 .. $#{ $a->plain } );

=cut

my $entrywise_check = sub {
    my ( $self, $other ) = @_;

    croak 'Missing parameter in vector arithmetic' unless defined $other;

    my @self  = @{ $self };

    $other = [ ($other) x @self ] unless ref $other;

    my @other = @{ $other };

    croak 'Vectors are not of the same length'
        unless @self == @other;

    return $other;
};

=head2 C<subtract>

This method is used to overload the C<-> operator.

Perform entrywise subtraction of components between two vectors.

=cut

sub subtract {
    my ( $self, $other, $swap ) = @_;

    if ($swap) {
        # Negation
        return Popsicle::Vector->new( map { -$_ } @{$self} ) unless $other;
        croak sprintf '%s - Vector: operation not allowed',
            ref($other) // 'Scalar';
    }

    $other = $entrywise_check->(@_);
    return Popsicle::Vector->new(
        map { $self->[$_] - $other->[$_] } 0 .. $#$self );
}

=head2 C<add>

This method is used to overload the C<+> operator.

Perform entrywise addition of components between two vectors.

=cut

sub add {
    my ( $self, $other, $swap ) = @_;

    croak sprintf( '%s + Vector: operation not allowed',
        ref($other) // 'Scalar' )
        if $other && $swap;

    $other = $entrywise_check->(@_);
    return Popsicle::Vector->new(
        map { $self->[$_] + $other->[$_] } 0 .. $#$self );
}

=head2 C<multiply>

This method is used to overload the C<*> operator.

Perform entrywise multiplication of components between two vectors.

Other types of multiplication (eg. so-called dot-product or cross-product) are
not yet implemented.

=cut

sub multiply {
    my ( $self, $other, $swap ) = @_;

    croak sprintf( '%s * Vector: operation not allowed',
        ref($other) // 'Scalar' )
        if $other && $swap;

    $other = $entrywise_check->(@_);
    return Popsicle::Vector->new(
        map { $self->[$_] * $other->[$_] } 0 .. $#$self );
}

=head2 C<divide>

This method is used to overload the C</> operator.

Perform entrywise division of components between two vectors.

Using this operation with an divisor vector with any component equal to zero
is an error and will throw an exception.

=cut

sub divide {
    my ( $self, $other, $swap ) = @_;

    croak sprintf( '%s / Vector: operation not allowed',
        ref($other) // 'Scalar' )
        if $other && $swap;

    $other = $entrywise_check->(@_);
    croak 'Illegal division by zero in entrywise division' if grep { !$_ } @$other;

    return Popsicle::Vector->new(
        map { $self->[$_] / $other->[$_] } 0 .. $#$self );
}

=head2 C<abs>

This method is used to overload the C<abs> operator.

Returns a new vector in which every component is equal to the absolute value
of the correspondent component of the original vector.

This method takes no arguments.

=cut

sub abs { Popsicle::Vector->new( map { CORE::abs } @{ +shift } ) }

1;
