package Popsicle::Slope;

use strict;
use warnings;

use Carp qw( carp croak );
use Scalar::Util qw( blessed );
use Ref::Util qw( is_arrayref );

# It might make more sense to have the arguments for slopes as [ y, x ],
# but that might get confusing, since everything else is [ x, y ]
sub x : lvalue { shift->[0] }
sub y : lvalue { shift->[1] }

my $arithmetic = sub {
    my ( $op, $self, $other, $swap ) = @_;

    croak 'Missing parameter in slope arithmetic' unless defined $other;

    my $value = $self->value;
    ( $value, $other ) = ( $other, $value ) if $swap;

    for ($op) {
        return $value + $other if $_ eq '+';
        return $value - $other if $_ eq '-';
        return $value * $other if $_ eq '*';
        return $value / $other if $_ eq '/';
    }

    croak 'Unknown operator: ' . $op;
};

use overload
    'cmp' => sub {
        carp 'Experimental behaviour for string comparison for slopes';
        goto \&compare;
    },
    '<=>' => \&compare,
    '0+'  => \&value,
    'neg' => sub { -$_[0]->[1] / $_[0]->[0] },
    '-'   => sub { $arithmetic->( '-', @_ ) },
    '+'   => sub { $arithmetic->( '+', @_ ) },
    '*'   => sub { $arithmetic->( '*', @_ ) },
    '/'   => sub { $arithmetic->( '/', @_ ) },
;

use namespace::autoclean;

sub new {
    my $class = shift;
    $class = ref $class if blessed $class;

    my @array = is_arrayref $_[0] ? @{ +shift // [] } : @_;
    croak 'Cannot create slope without components' unless @array;

    # TODO
    croak 'Slopes with more than 2 dimensions not supported yet'
        unless @array == 2;

    return bless [@array], $class;
}

sub compare {
    my ( $self, $other, $swap ) = @_;


    croak sprintf( '%s + Slope: operation not allowed',
        ref($other) // 'Scalar' )
        if $other && $swap;

    croak 'Missing parameter in slope arithmetic' unless defined $other;

    # Comparison against scalar done directly on value
    return $self->value <=> $other unless ref $other;

    my @self  = @{ $self };
    my @other = @{ $other };

    return $self[1] * $other[0] <=> $self[0] * $other[1];
}

sub value {
    my ( $self ) = @_;
    return $self->y / $self->x;
}

1;
