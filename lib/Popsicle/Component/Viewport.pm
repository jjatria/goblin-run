package Popsicle::Component::Viewport;

use Moo;

with qw(
    Popsicle::Role::Dimensions
    Popsicle::Role::Position
);

1;
