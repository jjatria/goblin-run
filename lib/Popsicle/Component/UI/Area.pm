package Popsicle::Component::UI::Area;

use Moo::Role;
use Carp 'croak';

use Log::Any '$log';

with qw(
    Popsicle::Role::Dimensions
    Popsicle::Role::Handles::Event
    Popsicle::Role::Position
);

sub on_enter {}
sub on_leave {}
sub on_click {}

sub on_event {
    my ($self, $event ) = @_;

    return unless $event->isa('Popsicle::Event::Mouse');

    # Convert mouse position to tile position
    my $stage = $event->stage;
    my $xy = $stage->to_stage_coordinates(
        $event->position, $self->position_is_relative );

    unless ( $self->in_bounds($xy) ) {
        $self->on_leave($event) if delete $self->{_mouse_hover};
        return;
    }

    if ( $event->isa('Popsicle::Event::Mouse::Motion') ) {
        $self->on_enter($event) unless $self->{_mouse_hover};
    }
    elsif ( $event->isa('Popsicle::Event::Mouse::Released') ) {
        $self->on_click($event) if delete $self->{_mouse_click};
    }
    elsif ( $event->isa('Popsicle::Event::Mouse::Pressed') ) {
        $self->{_mouse_click} = 1;
    }

    $self->{_mouse_hover} = 1;
}

1;
