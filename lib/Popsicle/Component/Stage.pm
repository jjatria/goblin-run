package Popsicle::Component::Stage;

use Moo::Role;
with qw(
    Popsicle::Role::Dimensions
    Popsicle::Role::Entities
    Popsicle::Role::Emitter
    Popsicle::Role::Handles::Show
    Popsicle::Role::Handles::Move
    Popsicle::Role::Handles::Event
);

use Scalar::Util qw( refaddr );
use Carp qw( croak );

use Log::Any '$log';

use namespace::clean;

has game => (
    is => 'ro',
    required => 1,
    weak_ref => 1,
);

sub on_event {
    my ( $self, $event ) = @_;
    $event->stage($self);
    return $self->forward( event => $event );
}

sub on_show {
    my ( $self, $event ) = @_;
    $event->stage($self);
    return $self->forward( show => $event );
}

sub on_move {
    my ( $self, $event ) = @_;
    $event->stage($self);
    return $self->forward( move => $event );
}

# Convert coordinates between raw and stage coordinates

sub to_stage_coordinates {
    my ( $self, $coord, $relative ) = @_;
    return $coord;
}

sub to_raw_coordinates {
    my ( $self, $coord, $relative ) = @_;
    return $coord;
}

# These methods are very SDL-specific for now,
# need to figure out how much more general they need to be

use SDL::Mouse;
use SDL::Events ();

sub hide_cursor {
    # Very SDL-specific for now, needs to be more general
    SDL::Mouse::show_cursor( SDL::Events::SDL_DISABLE );
}

sub show_cursor {
    # Very SDL-specific for now, needs to be more general
    use SDL::Mouse;
    use SDL::Events ':state';
    SDL::Mouse::show_cursor( SDL::Events::SDL_ENABLE );
}

sub toggle_cursor {
    use SDL::Mouse;
    use SDL::Events ':state';
    SDL::Mouse::show_cursor( SDL::Events::SDL_QUERY )
        ? hide_cursor() : show_cursor();
}

1;
