package Popsicle::Event::System::Focus;

use Moo;
extends 'Popsicle::Event::System';

has gain        => ( is => 'ro' );
has active      => ( is => 'ro' );
has input_focus => ( is => 'ro' );
has mouse_focus => ( is => 'ro' );

1;
