package Popsicle::Event::SDL;

use Moo;
use Popsicle::Types 'Vector';

extends 'Popsicle::Event';

has external => (
    is => 'ro',
    handles => [qw(
        motion_x
        motion_y
        type
        button_button
        key_sym
    )],
);

sub position {
    Vector->coerce([ map { $_->motion_x, $_->motion_y } shift->external ]);
}

1;
