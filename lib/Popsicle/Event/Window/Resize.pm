package Popsicle::Event::Window::Resize;

use Moo;
extends 'Popsicle::Event::Window';

with qw(
    Popsicle::Role::Dimensions
);

1;
