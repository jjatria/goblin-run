package Popsicle::Event::Key;

use Moo;
extends 'Popsicle::Event';

has symbol => (
    is => 'ro',
    required => 1,
);

1;
