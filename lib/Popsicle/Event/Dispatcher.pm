package Popsicle::Event::Dispatcher;

use Moo::Role;
use Carp 'croak';
use Log::Any '$log';
with 'Beam::Emitter';

requires 'dispatch';

use Scalar::Util 'blessed';

around emit => sub {
    my ( $orig, $self ) = ( shift, shift );

    my $event = $self->$orig(@_);
    return $event if $event->is_stopped || ! $event->isa('Popsicle::Event');

    my @names;

    my $chain = '';
    for my $part ( split /::/, ref $event ) {
        $chain .= '::' if $chain;
        $chain .= $part;
        push @names, $chain;
    }

    EVENTNAME: while ( my $name = shift @names ) {
        next unless exists $self->_listeners->{$name};

        # don't use $self->_listeners->{$name} directly, as callbacks may unsubscribe
        # from $name, changing the array, and confusing the for loop
        my @listeners = @{ $self->_listeners->{$name} };

        for my $listener ( @listeners  ) {
            $listener->callback->( $event );
            last EVENTNAME if $event->is_stopped;
        }
    }

    return $event;
};

1;
