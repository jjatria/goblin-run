package Popsicle::Event::Show;

use Moo;
extends 'Popsicle::Event';

has [qw( app dt )] => ( is => 'ro' );

1;
