package Popsicle::Event::Dispatcher::SDL;

use Moo;
use Log::Any '$log';

use Popsicle::Events;
use SDL::Events qw( :app :type :button );

with qw(
    Popsicle::Event::Dispatcher
);

has app => (
    is => 'ro',
    required => 1,
    weak_ref => 1,
);

sub BUILD {
    my ( $self, $args ) = @_;

    $self->app->add_event_handler(
        $self->curry::weak::dispatch
    );

    $self->app->add_move_handler(
        $self->$curry::weak(
            sub {
                my ( $s, $step, $app, $dt ) = shift;

                $s->emit(
                    move => (
                        class => 'Popsicle::Event::Move',
                        step  => $step,
                        app   => $app,
                        dt    => $dt,
                    ),
                );
            }
        )
    );

    $self->app->add_show_handler(
        $self->$curry::weak(
            sub {
                my ( $s, $dt, $app ) = @_;

                $s->emit(
                    show => (
                        class => 'Popsicle::Event::Show',
                        dt    => $dt,
                        app   => $app,
                    ),
                );

                $app->update;
            }
        )
    );
}

sub dispatch {
    my ( $self, $e ) = @_;
    my $type = $e->type;

    return $self->emit(
        event => (
            class    => 'Popsicle::Event::Mouse::Motion',
            position => [ $e->motion_x, $e->motion_y ],
        )
    ) if $type == SDL_MOUSEMOTION;

    if ( $type == SDL_MOUSEBUTTONDOWN ) {
        my $button = $e->button_button;

        return $self->emit(
            event => (
                class    => 'Popsicle::Event::Mouse::Wheel',
                position => [ $e->button_x, $e->button_y ],
                offset   => $button == SDL_BUTTON_WHEELUP ? 1 : -1,
            )
        ) if $button == SDL_BUTTON_WHEELDOWN || $button == SDL_BUTTON_WHEELUP;

        $log->debugf( 'Middle button' ) if $button == SDL_BUTTON_MIDDLE;

        return $self->emit(
            event => (
                class    => 'Popsicle::Event::Mouse::Pressed',
                position => [ $e->button_x, $e->button_y ],
                button   => $button,
            )
        );
    }

    return $self->emit(
        event => (
            class    => 'Popsicle::Event::Mouse::Released',
            position => [ $e->button_x, $e->button_y ],
            button   => $e->button_button,
        )
    ) if $type == SDL_MOUSEBUTTONUP;

    return $self->emit(
        event => (
            class  => 'Popsicle::Event::Key::Pressed',
            symbol => $e->key_sym,
        )
    ) if $type == SDL_KEYDOWN;

    return $self->emit(
        event => (
            class  => 'Popsicle::Event::Key::Released',
            symbol => $e->key_sym,
        )
    ) if $type == SDL_KEYUP;

    if ( $type == SDL_VIDEORESIZE ) {
        my $event = $self->emit(
            event => (
                class      => 'Popsicle::Event::Window::Resize',
                dimensions => [ $e->resize_w, $e->resize_h ],
            )
        );

        $log->warnf('Resize to %s', $event->dimensions);

        return $event;
    }

    return $self->emit(
        event => ( class => 'Popsicle::Event::System::Quit' )
    ) if $type == SDL_QUIT;

    return $self->emit(
        event => (
            class       => 'Popsicle::Event::System::Focus',
            gain        => $e->active_gain,
            active      => $e->active_state & SDL_APPACTIVE,
            mouse_focus => $e->active_state & SDL_APPMOUSEFOCUS,
            input_focus => $e->active_state & SDL_APPINPUTFOCUS,
        )
    ) if $type == SDL_ACTIVEEVENT;

    $log->warn( 'Unhandled event' );

    return $self->emit(
        event => (
            class  => 'Popsicle::Event::Unknown',
            source => $e,
        )
    );
}

1;
