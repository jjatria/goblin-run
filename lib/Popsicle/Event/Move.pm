package Popsicle::Event::Move;

use Moo;
extends 'Popsicle::Event';

has [qw( app step dt )] => ( is => 'ro' );

1;
