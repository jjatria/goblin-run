package Popsicle::Event::Mouse::Wheel;

use Moo;
extends 'Popsicle::Event::Mouse';

has offset => ( is => 'ro' );

1;
