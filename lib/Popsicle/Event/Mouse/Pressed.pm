package Popsicle::Event::Mouse::Pressed;

use Moo;
extends 'Popsicle::Event::Mouse';

has button => ( is => 'ro' );

1;
