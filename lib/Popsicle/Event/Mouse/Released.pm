package Popsicle::Event::Mouse::Released;

use Moo;
extends 'Popsicle::Event::Mouse';

has button => ( is => 'ro' );

1;
