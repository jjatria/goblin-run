package Popsicle::Event::Unknown;

use Moo;
extends 'Popsicle::Event';

has source => ( is => 'ro', required => 1 );

1;
