package Popsicle::Types;

use Type::Library
    -base,
    -declare => qw(
        Grid
        Stage
        Map
        Vector
    );

use Type::Utils -all;
use Types::Standard qw( Str ArrayRef );

use namespace::clean;

class_type Grid, { class => 'Popsicle::Map::Grid' };

class_type Map, { class => 'Popsicle::Map' };

class_type Vector, { class => 'Popsicle::Vector' };

role_type Stage, { role => 'Popsicle::Component::Stage' };

coerce Vector,
    from ArrayRef, via {
        require Popsicle::Vector;
        Popsicle::Vector->new( @{$_} );
    },
    from Str, via {
        require Popsicle::Vector;
        Popsicle::Vector->from_string($_);
    };

1;
