package Popsicle::UI::Element;

use Moo;
use Carp 'croak';
use Scalar::Util 'refaddr';
use Hash::Ordered;

use Log::Any '$log';

has child_roster => (
    is      => 'ro',
    lazy    => 1,
    default => sub { Hash::Ordered->new },
);

sub children { shift->child_roster->values }

sub add_child {
    my ( $self, $child ) = @_;

    croak 'Cannot remove undefined element' unless $child;

    my $refaddr = refaddr $child;

    croak 'Cannot add same element twice'
        if $self->child_roster->exists($refaddr);;

    $self->child_roster->set( $refaddr => $child );

    return $child;
}

sub remove_child {
    my ( $self, $target ) = @_;

    my $refaddr = refaddr $target;

    croak 'Cannot remove undefined element' unless $target;

    # Method is idempotent
    my $child = $self->child_roster->delete($refaddr) or return $target;

    $child->redraw($self) if $child->can('redraw');

    return $child;
}

1;
