package Popsicle::UI::Box;

use Moo;
use Log::Any '$log';
use Popsicle::Vector;
use Types::Standard qw( Int Num Enum );
use Popsicle::Types 'Vector';

extends 'Popsicle::UI::Element';

with qw(
    Popsicle::Role::Dimensions
    Popsicle::Role::Position
);

has '+position_is_relative' => ( default => 0 );

has orient => (
    is => 'ro',
    required => 1,
    isa => Enum[qw( horizontal vertical )],
);

has align => (
    is => 'ro',
    required => 1,
    isa => Enum[qw( stretch )],
    default => 'stretch',
);

has flex => (
    is => 'ro',
    default => 0,
    isa => Int,
);

has [qw(
       top_margin
    bottom_margin
      left_margin
     right_margin
)] => ( is => 'ro', isa => Num, default => 0 );

around BUILDARGS => sub {
    my ( $orig, $class ) = ( shift, shift );
    my $args = $class->$orig(@_);

    my $dimensions = delete $args->{dimensions};
    my $width      = delete $args->{width};
    my $height     = delete $args->{height};

    $args->{original}{dimensions} = $dimensions if $dimensions;
    $args->{original}{dimensions} //= [ $width // 0, $height // 0 ];

    $args->{original}{dimensions}
        = Vector->coerce( $args->{original}{dimensions} );

    $args->{dimensions} = $args->{original}{dimensions}->clone;

    my $margin = delete $args->{margin} // 0;
    $args->{top_margin}    //= $margin;
    $args->{bottom_margin} //= $margin;
    $args->{left_margin}   //= $margin;
    $args->{right_margin}  //= $margin;

    return $args;
};

sub BUILD {
    my ( $self, $args ) = @_;
    $self->{original} = delete $args->{original};
}

after [qw( add_child remove_child )] => sub { shift->calculate_layout };

sub calculate_layout {
    my ($self) = @_;
    my @children = $self->children or return;

    my $pos = $self->position;
    my $dim = $self->dimensions;

    my ( $this_dim, $other_dim )
        = $self->orient ne 'vertical' ? ( 'x', 'y' ) : ( 'y', 'x' );

    my ( $this_first_margin, $other_first_margin )
        = $self->orient ne 'vertical'
        ? ( 'left_margin', 'top_margin' )
        : ( 'top_margin',  'left_margin' );

    my ( $this_second_margin, $other_second_margin )
        = $self->orient ne 'vertical'
        ? ( 'right_margin',  'bottom_margin' )
        : ( 'bottom_margin', 'right_margin' );

    my $used_space = 0;
    for ( @children ) {
        $used_space += $_->{original}{dimensions}->$this_dim;
    }

    my $free_space = $dim->$this_dim - $used_space;

    my $total_flex = 0;
    $total_flex += $_->flex for @children;

    my $flex_unit = $total_flex ? $free_space / $total_flex : 0;

    my $current = $pos->clone;
    for my $i ( 0 .. $#children ) {
        my $child = $children[$i];

        # Calculate dimensions
        my $d = $child->{original}{dimensions}->clone;
        $d->$this_dim += $flex_unit * $child->flex if $child->flex;

        # TODO: Only "stretch" alignment supported for now.
        $d->$other_dim = $dim->$other_dim if $child->align eq 'stretch';

        $d->$this_dim -= ( $child->$this_first_margin + $child->$this_second_margin );
        $d->$other_dim -= ( $child->$other_first_margin + $child->$other_second_margin );

        $child->dimensions($d);

        # Calculate position

        $current->$this_dim  += $child->$this_first_margin;

        my $pos = $current->clone;
        $pos->$other_dim += $child->$other_first_margin;

        $child->position($pos);

        $current->$this_dim += $child->dimensions->$this_dim;
        $current->$this_dim += $child->$this_second_margin;
    }
    continue {
        $children[$i]->calculate_layout;
    }
}

1;
