package Popsicle::UI::VBox;

use Moo;

extends 'Popsicle::UI::Box';

has '+orient' => ( default => 'vertical' );

1;
