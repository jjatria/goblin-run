package Popsicle::UI::HBox;

use Moo;

extends 'Popsicle::UI::Box';

has '+orient' => ( default => 'horizontal' );

1;
