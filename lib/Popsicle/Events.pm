package Popsicle::Events;

use Import::Into;

sub import {
    my $target = caller;
    $_->import::into($target) for qw(
        Popsicle::Event
        Popsicle::Event::Key
        Popsicle::Event::Key::Released
        Popsicle::Event::Key::Pressed
        Popsicle::Event::Mouse
        Popsicle::Event::Mouse::Motion
        Popsicle::Event::Mouse::Wheel
        Popsicle::Event::Mouse::Released
        Popsicle::Event::Mouse::Pressed
        Popsicle::Event::System::Focus
        Popsicle::Event::System::Quit
        Popsicle::Event::Window::Resize
        Popsicle::Event::Unknown
    );
}

1;
