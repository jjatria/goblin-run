package Popsicle::Map::Grid;

use Moo;

use Carp 'croak';
use Ref::Util 'is_arrayref';
use Popsicle::Types qw( Vector );

use namespace::clean;

extends 'Graph::Base::Grid::King';

has '+node_class' => ( default => 'Popsicle::Map::Cell' );

around [qw( directions neighbourhood euclidean_neighbourhood )] => sub {
    my $orig = shift;
    my $self = shift;
    return map { Vector->coerce($_) } $self->$orig(@_);
};

sub trace_line {
    my ( $self, $start, $end ) = @_;

    # Only 2D grids are supported
    croak 'Cannot trace line without valid coords'
        unless is_arrayref $start && scalar @{$start} >= 2
        &&     is_arrayref $end   && scalar @{$end}   >= 2;

    $start = Vector->coerce($start);
    $end   = Vector->coerce($end);

    # Absolute difference with negated y-component
    my $delta = abs( $end - $start ) * [ 1, -1 ];

    my $step = Vector->coerce([
        $start->x < $end->x ? 1 : -1,
        $start->y < $end->y ? 1 : -1,
    ]);

    my $error = $delta->x + $delta->y;
    my $double_error;

    my @coords;

    my $here = $start;

    while (1) {
        push @coords, $here;
        last if $here eq $end;

        $double_error = 2 * $error;

        if ( $double_error >= $delta->y ) {
            $error += $delta->y;
            $here += [ $step->x, 0 ];
        }

        if ( $double_error <= $delta->x ) {
            $error += $delta->x;
            $here += [ 0, $step->y ];
        }
    }

    return @coords;
}

1;

=pod

A grid is a collection of cells, which may be connected between them.

A grid is represented like a graph.

Grid cells make reference to tiles, which are defined in tilesets.

=cut
