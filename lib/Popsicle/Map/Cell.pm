package Popsicle::Map::Cell;

use Moo;

extends 'Graph::Base::Node::Cell';

use Types::Standard qw( Bool );
use Popsicle::Types qw( Vector );

use namespace::clean;

has '+name' => ( lazy => 1, default => sub { $_[0]->coord->to_string } );

has '+coord' => ( isa => Vector, coerce => 1 );

sub x { shift->coord->x }
sub y { shift->coord->y }

has seen => ( is => 'rw', isa => Bool, default => 0 );

has visible => (
    is      => 'rw',
    isa     => Bool,
    default => 0,
    trigger => sub { $_[0]->seen(1) if $_[1] },
);

has tile => ( is => 'rw', weak_ref => 1 );

1;

=pod

A cell exists within a grid. If a grid is a graph, cells are the graphs nodes.

Cell store a (weak) reference to a tile, and hold some cell-specific attributes:

* is the cell currently visible

* has the cell been explored

* position

* is the cell passable

=cut
