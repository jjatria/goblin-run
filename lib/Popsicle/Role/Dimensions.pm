package Popsicle::Role::Dimensions;

use Moo::Role;

use Carp 'croak';
use Scalar::Util 'blessed';
use Popsicle::Types qw( Vector );

use namespace::clean;

has dimensions => (
    is      => 'rw',
    isa     => Vector,
    coerce  => 1,
    default => sub { Vector->coerce([ 0, 0 ]) },
);

sub width  : lvalue { shift->dimensions->x };
sub height : lvalue { shift->dimensions->y };

=head2 centre

    my $vector = $obj->centre;
    my $vector = $obj->centre($absolute);

Returns a vector representing the coordinate at the centre of this object.

By default, this will be an absolute coordinate, which includes the object's
position if it has one. Passing a false value disables this and returns a
relative coordinate.

=cut

sub centre {
    my ( $self, $relative ) = @_;
    my $coord = $self->dimensions / 2;
    return !$relative && $self->can('position')
        ? $self->position + $coord : $coord;
}

=head2 rectangle

    my $vector = $obj->rectangle;
    my $vector = $obj->rectangle($absolute);

Returns a vector representing the rectangle around this object, where the
first set of components will be the object's position, and the latter will be
the object's dimensions. If the object has no position, the first components
will be set to zero.

By default, this will be an absolute rectangle, which includes the object's
position if it has one. Passing a false value disables this and returns a
relative rectangle, where the first components will always be zero.

Note that this method only makes sense for 2-dimensional objects.

=cut

sub rectangle {
    my ( $self, $relative ) = @_;
    my $dim = $self->dimensions;
    my $pos = !$relative && $self->can('position')
        ? $self->position : $self->position->zero;
    return Vector->coerce([ @{ $pos }, @{ $dim + $pos } ]);
}

sub area {
    my ( $self ) = @_;

    # A box with a dimension with no components has an undefined area
    my @components = $self->dimensions->components or return;

    my $area = 1;
    $area *= $_ for @components;

    return $area;
}

sub in_bounds {
    my $self  = shift;
    my $coord = Vector->coerce(shift);

    my $relative = $self->can('position') ? $coord - $self->position : $coord;
    my $dims     = $self->dimensions;

    for ( 0 .. $#{$relative} ) {
        return if $relative->[$_] < 0;
        return if $relative->[$_] > $dims->[$_];
    }

    return 1;
}

sub overlaps_with {
    my $self = shift;
    my $box  = shift;

    croak 'Cannot check overlap without dimensions'
        unless blessed $box && $box->DOES('Popsicle::Role::Dimensions');

    # Origin of reference box
    my $a1 = $self->can('position')
        ? $self->position : Vector->coerce([0, 0]);

    # Origin of target box
    my $b1 = $box->can('position')
        ? $box->position : Vector->coerce([0, 0]);

    # End of reference box
    my $a2 = $a1 + $self->dimensions;

    # End of target box
    my $b2 = $b1 + $box->dimensions;

    return 0
        if $a1->x >= $b2->x   # Reference box to the right of target
        || $b1->x >= $a2->x;  # Target box to the right of reference

    return 0
        if $a1->y >= $b2->y   # Reference box is below target box
        || $b1->y >= $a2->y;  # Target box is below reference box

    return 1;
}

1;

=pod

A class with dimensions is an orthotope. It is the object defined by projecting
a magnitude in one dimension along multiple other dimensions at right angles
to itself.

In one dimension it is a line; in 2D it is a rectangle; in 3D it is a
parallelogram; etc.

=cut
