package Popsicle::Role::Move;

use Moo::Role;

use Popsicle::Types qw( Vector );

use namespace::clean;

with qw( Popsicle::Role::Position );

has velocity => (
    is      => 'rw',
    isa     => Vector,
    coerce  => 1,
    lazy    => 1,
    clearer => 'stop_moving',
    default => sub { shift->position * 0 },
);

sub dx : lvalue { shift->velocity->x };
sub dy : lvalue { shift->velocity->y };

sub speed { shift->velocity->length }

sub move {
    my ( $self, $step ) = @_;
    $step //=  1;

    my $speed = $self->speed;
    return $self unless $speed;

    my $velocity = $self->velocity;

    if ( $step != 1 ) {
        my $step_size = $speed * $step;
        $velocity = $self->velocity->normalise * $step_size
    }

    $self->position( $self->position + $velocity );

    return $self,
}

1;

=pod

A role for something that moves.

If it moves, it implies it has a position.

=cut
