package Popsicle::Role::Handles::Move;

use Moo::Role;
use namespace::clean;

requires 'on_move';

1;
