package Popsicle::Role::Handles::Event;

use Moo::Role;
use namespace::clean;

requires 'on_event';

1;
