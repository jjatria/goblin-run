package Popsicle::Role::Handles::Show;

use Moo::Role;
use namespace::clean;

requires 'on_show';

1;
