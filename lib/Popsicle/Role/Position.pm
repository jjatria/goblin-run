package Popsicle::Role::Position;

use Moo::Role;

use Popsicle::Types qw( Vector );

use namespace::clean;

has position => (
    is      => 'rw',
    isa     => Vector,
    coerce  => 1,
    default => sub { [ 0, 0 ] },
);

sub x : lvalue { shift->position->x };
sub y : lvalue { shift->position->y };

has position_is_relative => ( is => 'ro', default  => 1 );

1;

=pod

A role for something with a position.

=cut
