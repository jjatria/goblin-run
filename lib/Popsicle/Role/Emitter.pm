package Popsicle::Role::Emitter;

use Moo::Role;
with 'Beam::Emitter';

use Popsicle::Event::SDL;
use Popsicle::Event::Move;
use Popsicle::Event::Show;

sub forward {
    my ( $self, $name, $event ) = @_;

    return $event unless exists $self->_listeners->{$name};

    # Don't use $self->_listeners->{$name} directly, as callbacks
    # may unsubscribe from $name, changing the array, and confusing
    # the for loop
    my @listeners = @{ $self->_listeners->{$name} };

    local $event->{emitter} = $self;

    for my $listener ( @listeners  ) {
        $listener->callback->( $event );
        last if $event->is_stopped;
    }

    return $event;
}

around emit => sub {
    my ( $orig, $self ) = ( shift, shift );
    return $self->$orig( shift, class => 'Popsicle::Event', @_ );
};

1;
