package Popsicle::Role::Entities;

use curry;
use Moo::Role;
use Carp 'croak';
use Log::Any '$log';
use Scalar::Util 'refaddr';

sub entities { values %{ shift->{entities} // {} } };

sub add_entity {
    my ( $self, $entity ) = @_;

    my $refaddr = refaddr $entity;

    croak 'Cannot add undefined entity' unless $entity;

    my @unsubs;

    push @unsubs, $self->on( event => $entity->curry::weak::on_event )
        if $entity->DOES('Popsicle::Role::Handles::Event');

    push @unsubs, $self->on( show => $entity->curry::weak::on_show )
        if $entity->DOES('Popsicle::Role::Handles::Show');

    push @unsubs, $self->on( move => $entity->curry::weak::on_move )
        if $entity->DOES('Popsicle::Role::Handles::Move');

    $self->{entity_unsubscriber}{$refaddr} = sub { $_->() for @unsubs };

    return $self->{entities}{$refaddr} = $entity;
}

sub remove_entity {
    my $self = shift;

    croak 'Cannot remove undefined entity' unless @_;

    my $refaddr = refaddr shift;
    my $entity = delete $self->{entities}{$refaddr} or return;

    ( delete $self->{entity_unsubscriber}{$refaddr} )->()
        if $self->{entity_unsubscriber}{$refaddr};

    delete $self->{entity_unsubscriber}
        unless %{ $self->{entity_unsubscriber} // {} };


    $entity->redraw($self) if $entity->can('redraw');

    delete $self->{entities} unless %{ $self->{entities} // {} };

    return $entity;
}

1;
