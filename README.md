To install, run

    carton install --cached

To execute, run

    carton exec script/game

You'll also need

    libexpat1-dev
