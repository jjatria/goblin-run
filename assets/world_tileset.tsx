<?xml version="1.0" encoding="UTF-8"?>
<tileset name="Dungeon Tileset" tilewidth="32" tileheight="32" tilecount="80" columns="10">
 <image source="tileset_dungeon.png" width="320" height="256"/>
 <terraintypes>
  <terrain name="Floor" tile="20"/>
  <terrain name="Wall" tile="0"/>
  <terrain name="Void" tile="68"/>
 </terraintypes>
 <tile id="44">
  <properties>
   <property name="impassable" type="bool" value="true"/>
  </properties>
 </tile>
</tileset>
