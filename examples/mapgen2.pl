#!/usr/bin/perl

use 5.12.0;
use strict;
use warnings;

use Log::Any::Adapter 'Stderr';
use Local::LevelBuilder::ConnectedRooms;

use Term::Caca;
use Term::Caca::Constants qw( :colors :events );

my $max_rooms = 50;
my $min_door_dist = 2;

my $term = Term::Caca->new;

my $grid = Local::LevelBuilder::ConnectedRooms->new->generate(
    dimensions => $term->canvas_size,
);

$term->clear;
$term->set_color( WHITE, BLACK );

draw_grid($grid);

$term->refresh;

hold();
exit;

sub draw_grid {
    my $grid = shift;

    my ( $width, $height ) = @{ $grid->dimensions };

    $grid->lock;

    for my $x ( 0 .. $width-1 ) {
        for my $y ( 0 .. $height-1 ) {
            my $cell = $grid->get_cell([ $x, $y ]) or next;
            my $char = $cell->is_passable ? '.' : '#';
            $term->char( [ $x, $y ], $char );
        }
    }

    $grid->unlock;
}

sub draw_room {
    my $box = shift;

    my $pos = $box->position;
    my ( $width, $height ) = @{ $box->size };

    for my $x ( 0 .. $width-1 ) {
        for my $y ( 0 .. $height-1 ) {
            $term->char( $pos + [ $x, $y ], '.' );
        }
    }

    for my $x ( -1 .. $width ) {
        for my $y ( -1, $height ) {
            $term->char( $pos + [ $x, $y ], '#' );
        }
    }

    for my $x ( -1, $width ) {
        for my $y ( 0 .. $height-1 ) {
            $term->char( $pos + [ $x, $y ], '#' );
        }
    }
}

sub draw_wall {
    my $box = shift;
    my $wall = shift;

    my $pos = $box->position;
    my ( $width, $height ) = @{ $box->size };

    # North / South
    if ( $wall->x == 0 ) {
        for my $x ( -1 .. $width ) {
            $term->char( $pos + [ $x, $wall->y > 0 ? $height : -1 ], '#' );
        }
    }

    # East / West
    else {
        for my $y ( -1 .. $height ) {
            $term->char( $pos + [ $wall->x > 0 ? $width : -1, $y ], '#' );
        }
    }
}

sub hold {
    my $event = $term->wait_for_event( KEY_PRESS | QUIT, -1 );

    exit if $event->isa( 'Term::Caca::Event::Quit' )
        or $event->char eq 'q';
}
