#!/usr/bin/perl

use strict;
use warnings;

use Log::Log4perl qw(:easy);
Log::Log4perl->easy_init($TRACE);
use Log::Any::Adapter 'Log4perl';

use Popsicle::Vector;
use Popsicle::Visibility::BeveledCells;

my $map = [
    #    0 1 2 3 4 5 6 7 8 9 0 1 2 3 4
    [qw( . . . . . . . . . . . . . . .  )], #  0
    [qw( . . . . . . . . . . . . . . .  )], #  1
    [qw( . . . . . . . . . . . . . . .  )], #  2
    [qw( . . . . . . . . . . . . . . .  )], #  3
    [qw( . . . . . . . . . . . . . . .  )], #  4
    [qw( . . . + . . . . . . . . . . .  )], #  5
    [qw( . . . + . . . + . . . . . . .  )], #  6
    [qw( . . . . . . . @ . . + . . . .  )], #  7
    [qw( . . . + . . . . . . . . . . .  )], #  8
    [qw( . . . + . . . + . . . . . . .  )], #  9
    [qw( . . . + + + . . . . . . . . .  )], # 10
    [qw( . . . . . . . . . . . . . . .  )], # 11
    [qw( . . . . . . . . . . . . . . .  )], # 12
    [qw( . . . . . . . . . . . . . . .  )], # 13
    [qw( . . . . . . . . . . . . . . .  )], # 14
];

# my $map = [
#     #    0 1 2 3 4 5 6 7 8 9 0 1 2 3 4
#     [qw( @ . . + . . . . . . . . . . . )], #  0
#     [qw( . . . + . . . . . . . . . . . )], #  1
#     [qw( . . . + . . . . . . . . . . . )], #  2
#     [qw( . . . . . . . . . . . . . . . )], #  3
#     [qw( . . . . . . . . . . . . . . . )], #  4
#     [qw( . . . . . . . . . . . . . . . )], #  5
#     [qw( . . . . . . . . . . . . . . . )], #  6
#     [qw( . . . . . . . . . . . . . . . )], #  7
#     [qw( . . . . . . . . . . . . . . . )], #  8
#     [qw( . . . . . . . . . . . . . . . )], #  9
#     [qw( . . . . . . . . . . . . . . . )], # 10
#     [qw( . . . . . . . . . . . . . . . )], # 11
#     [qw( . . . . . . . . . . . . . . . )], # 12
#     [qw( . . . . . . . . . . . . . . . )], # 13
#     [qw( . . . . . . . . . . . . . . . )], # 14
# ];

my $coord;
PLAYER: for my $y ( 0 .. $#{$map} ) {
    for my $x ( 0 .. $#{ $map->[$y] } ) {
        if ( $map->[$y][$x] eq '@' ) {
            $coord = Popsicle::Vector->new( $x, $y );
            last PLAYER;
        }
    }
}

die unless $coord;

my $visible;

my $v = Popsicle::Visibility::BeveledCells->new(
    symmetric => 1,
    blocks_light => sub {
        my ( $x, $y ) = @{ +shift };
#         print "Checking if [$x,$y] is opaque\n";
        return ( $map->[$y][$x] // '' ) eq '+';
    },
    get_distance => sub {
        Popsicle::Vector->new(@_)->length
    },
    set_visible => sub {
        my ( $x, $y ) = @{ +shift };
#         print "[$x,$y] set to visible\n";
        $visible->{"$x,$y"} = 1;
    }
);

$v->compute( $coord, 6 );

for my $y ( 0 .. $#{$map} ) {
    my @line;
    for my $x ( 0 .. $#{ $map->[$y] } ) {
        if ( $visible->{"$x,$y"} ) {
            push @line, $map->[$y][$x];
        }
        else {
            push @line, ' ';
        }
    }
    print join( ' ', @line ) . "\n";
}
print "\n";
