#!/usr/bin/perl

use 5.12.0;

use Games::Maze;
use Term::Caca::Constants qw/ :colors :events /;
use Term::Caca;
use Popsicle::Types qw( Vector );
use Popsicle::Visibility::BeveledCells;

my $term = Term::Caca->new();

$term->title( 'maze' );

my $canvas = Vector->coerce( $term->canvas_size );
my $tiles  = ( $canvas / [ 3, 2 ] - 1 )->round;

my ( $width, $height ) = @{ $tiles };

use Graph::Base::Grid::Square;
use Graph::Base::Traversal::DepthFirst;
my $maze = Graph::Base::Traversal::DepthFirst->new(
        graph => Graph::Base::Grid::Square->new(
            dimensions => $tiles,
            lazy       => 0,
            complete   => 1,
        ),
    )
    ->traverse
    ->tree;

# display the maze itself
$term->set_color( LIGHTBLUE, BLACK );
draw_grid( $term, $maze );
$term->refresh;

my $pos = Vector->coerce([ 1, 1 ]);

while (1) {
    $term->set_color( RED, BLACK );
    $term->char( $pos, '@' );
    $term->refresh;
    $term->char( $pos, '.' );

    my $event = $term->wait_for_event( KEY_PRESS | QUIT, -1 );

    exit if $event->isa( 'Term::Caca::Event::Quit' )
         or $event->char eq 'q';

    my $moved;

    # move using the keypad (2, 4, 6, 8)
    my $delta = [ 0, 0 ];
    for ( $event->char ) {
        $delta = [  0,  1 ] if /2/;
        $delta = [  0, -1 ] if /8/;
        $delta = [ -1,  0 ] if /4/;
        $delta = [  1,  0 ] if /6/;
    }

    $delta = Vector->coerce($delta);
    if ($delta->length) {
        # Convert screen coordinates to grid coordinates
        # We subtract 0.1 to round down
        my $coord = ( $pos / [ 3, 2 ] - 0.1 )->round;
        my $cell = $maze->get_node( $coord );

        if ( $cell && $cell->has_link( $coord + $delta ) ) {
            # Convert grid coordinates to screen coordinates
            $pos += ( $delta * [ 3, 2 ] );
        }
    }
}

sub draw_grid {
    my ( $canvas, $grid ) = @_;

    for my $cell ( $grid->nodes ) {
        my $xy = Vector->coerce( $cell->coord );

        my $north = $cell->has_link( $xy - [ 0, 1 ] ) ? ' ' : '-';
        my $west  = $cell->has_link( $xy - [ 1, 0 ] ) ? ' ' : '|';

        # Screen coordinates
        my $coord = $xy * [ 3, 2 ];

        $canvas->char( $coord,             '+' );
        $canvas->char( $coord + [ 1, 0 ], $north );
        $canvas->char( $coord + [ 2, 0 ], $north );
        $canvas->char( $coord + [ 3, 0 ], '+' );

        $canvas->char( $coord + [ 0, 1 ], $west );

        $canvas->char( $coord + [ 0, 2 ], '+' );
        $canvas->char( $coord + [ 3, 2 ], '+' );
    }

    for my $x ( 0 .. $width-1 ) {
        $canvas->char( [ $x * 3 + 1, $height * 2 ], '-' );
        $canvas->char( [ $x * 3 + 2, $height * 2 ], '-' );
    }

    for my $y ( 0 .. $height-1 ) {
        $canvas->char( [ $width * 3, $y * 2 + 1 ], '|' );
    }
}
