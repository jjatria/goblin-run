#!/usr/bin/perl

use 5.12.0;

use Term::Caca;
use Term::Caca::Constants qw/ :colors :events /;
use Popsicle::Types qw( Vector );

my $term = Term::Caca->new();

$term->title( 'prefab demo' );

package Prefab {
    use Moo;
    use Popsicle::Types qw( Vector );

    with qw( Popsicle::Geometry::Positionable Popsicle::Geometry::Box );

    use constant PI    => 3.141_592_653_589_793;
    use constant PIRAD => PI / 180;

    has angle => (
        is => 'rw',
        default => 0,
        trigger => sub {
            if ( defined $_[1] ) {
                die 'Angle must be a multiple of 90' if $_[1] % 90;
                $_[0]->refresh_rotation;
            }
        },
    );

    has rotation => (
        is => 'ro',
        lazy => 1,
        clearer => 'refresh_rotation',
        default => sub {
            my $angle = $_[0]->angle * PIRAD;
            [
                [ cos($angle), -sin($angle) ],
                [ sin($angle),  cos($angle) ]
            ];
        },
    );

    has marker => (
        is => 'ro',
        default => sub { [ 1, 1 ] },
    );

    has grid => (
        is => 'ro',
    );

    sub transform {
        my ( $self, $coord ) = ( shift, Vector->coerce(shift) );
        my @rot = @{ $self->rotation };

        my $xx = $coord * $rot[0];
        my $yy = $coord * $rot[1];

        return Vector->coerce([ $xx->x + $xx->y, $yy->x + $yy->y ])->round;
    }
}

sub generate_prefab {
    my $dimensions = Vector->coerce(shift);

    require Graph::Base::Grid::Square;
    require Graph::Base::Traversal::DepthFirst;

    my $grid = Graph::Base::Traversal::DepthFirst->new(
            graph => Graph::Base::Grid::Square->new(
                dimensions => $dimensions,
                lazy       => 0,
            ),
        )
        ->traverse
        ->tree;

    return Prefab->new(
        position => [ 0, 0 ],
        size     => $dimensions,
        grid     => $grid,
    );
}

# display the maze itself

my $box = generate_prefab([ 5, 5 ]);

$term->set_color( LIGHTBLUE, BLACK );
draw_box( $term, $box );
$term->refresh;

while (1) {
    my $event = $term->wait_for_event( KEY_PRESS | QUIT, -1 );

    exit if $event->isa( 'Term::Caca::Event::Quit' )
         or $event->char eq 'q';

    my $moved;

    # move using the keypad (2, 4, 6, 8)
    my $delta = [ 0, 0 ];
    my $rotate;

    for ( $event->char ) {
        $delta = [  0,  1 ] if /2/;
        $delta = [  0, -1 ] if /8/;
        $delta = [ -1,  0 ] if /4/;
        $delta = [  1,  0 ] if /6/;
        $rotate = 1 if /5/;
    }

    my $draw;

    $delta = Vector->coerce($delta);
    if ($delta->length) {
        # Convert grid coordinates to screen coordinates
        $box->position( $box->position + ( $delta * [ 3, 2 ] ) );
        $draw = 1;
    }

    if ($rotate) {
        my $deg = $box->angle + 90;
        $box->angle( $deg % 360 );
        $draw = 1;
    }

    if ($draw) {
        $term->clear;
        draw_box( $term, $box );
        $term->refresh;
    }
}

sub draw_grid {
    my ( $canvas, $box ) = @_;

    my $grid = $box->grid;
    my ( $width, $height ) = @{ $box->size };

    # Tiles are rendered as 4 x 3 squares in text, minus the overlap
    #
    # +--+
    # |  |
    # +--+
    my $tile_size = Vector->coerce([ 3, 2 ]);

    # The shape of this tile might change if the box is sideways
    my $shifted_tile_size = $box->transform($tile_size)->abs;

    my $id = 0;
    for my $cell ( $grid->nodes ) {
        $id++;
        my $xy = Vector->coerce( $cell->coord );

        # Screen coordinates
        my $coord = $box->position + $box->transform( $xy * $shifted_tile_size );

        # The directions this grid knows about
        my @dirs = $grid->directions;

        # The internal directions in which this particular tile is connected
        my @links = map { $cell->has_link( $xy + $_ ) } @dirs;

        # The screen directions in which this particular tile is connected
        my @shifted = @links;
        my $rotation = int( $box->angle / 90 );
        push @shifted, shift @shifted for 1 .. $rotation;

        # say 'This cell connects to:';
        # say '[ N W S E ]';
        # say '[ ' . ( join ' ', map { $_ ? '*' : ' ' } @links ) . ' ] RAW';
        # say '[ ' . ( join ' ', map { $_ ? '*' : ' ' } @shifted ) . ' ] SHIFTED';

        # Can this be generalised?
        # It makes the prefab rotate more-or-less around its centre
        # Does not work for highly oblong prefabs (eg. 2 x 8)
        if ($rotation == 1) {
            $coord += [ ( $width - 1 ) * $tile_size->x, 0 ];
        }
        elsif ($rotation == 2) {
            $coord += $tile_size * [ $width - 1, $height - 1 ];
        }
        elsif ($rotation == 3) {
            $coord += [ 0, ( $height - 1 ) * $tile_size->y ];
        }

        $canvas->char( $coord,            '+' );
        $canvas->char( $coord + [ 1, 0 ], $shifted[0] ? ' ' : '-' );
        $canvas->char( $coord + [ 2, 0 ], $shifted[0] ? ' ' : '-' );
        $canvas->char( $coord + [ 3, 0 ], '+' );


        $canvas->char( $coord + [ 0, 1 ], $shifted[1] ? ' ' : '|' );

        $canvas->char( $coord + [ 0, 2 ], '+' );
        $canvas->char( $coord + [ 1, 2 ], $shifted[2] ? ' ' : '-' );
        $canvas->char( $coord + [ 2, 2 ], $shifted[2] ? ' ' : '-' );
        $canvas->char( $coord + [ 3, 2 ], '+' );

        $canvas->char( $coord + [ 3, 1 ], $shifted[3] ? ' ' : '|' );
    }
}

sub draw_box {
    my ( $canvas, $box ) = @_;

    my $pos = $box->position;
    my ( $w, $h ) = @{ $box->transform( $box->size )->abs };

    for my $x ( 0 .. $w-1 ) {
        # Top
        my $xy = $pos + [ $x * 3, 0 ];

        $canvas->char( $xy,            '+' );
        $canvas->char( $xy + [ 1, 0 ], '-' );
        $canvas->char( $xy + [ 2, 0 ], '-' );
        $canvas->char( $xy + [ 3, 0 ], '+' );

        # Bottom
        $canvas->char( $xy + [ 0, $h * 2 ], '+' );
        $canvas->char( $xy + [ 1, $h * 2 ], '-' );
        $canvas->char( $xy + [ 2, $h * 2 ], '-' );
        $canvas->char( $xy + [ 3, $h * 2 ], '+' );
    }

    for my $y ( 0 .. $h-1 ) {
        my $xy = $pos + [ 0, $y * 2 ];

        # Left
        $canvas->char( $xy + [ 0, 1 ], '|' );
        $canvas->char( $xy + [ 0, 2 ], '+' );

        # Right
        $canvas->char( $xy + [ $w * 3, 1 ], '|' );
        $canvas->char( $xy + [ $w * 3, 2 ], '+' );
    }

    draw_grid( $term, $box );
}
