#!/usr/bin/perl

use 5.12.0;

use utf8;
use Term::Caca;
use Graph::Base::Grid;
use Graph::Base::Grid::Square;
use Term::Caca::Constants qw/ :colors :events /;
use Popsicle::Types qw( Vector );
use Scalar::Util 'refaddr';

my $term = Term::Caca->new(  );

my ( $tiles_wide, $tiles_high ) = ( 40, 20 );

$term->title( 'prefab demo' );

package Box {
    use Moo;
    with qw( Popsicle::Geometry::Positionable Popsicle::Geometry::Box );
    sub transform { shift; return shift };
    has grid => ( is => 'rw', predicate => 1 );
    sub angle { 0 }
};

$term->set_color( LIGHTBLUE, BLACK );

require Moo::Role;
my $grid = Graph::Base::Grid::Square->new(
    dimensions => [ $tiles_wide, $tiles_high ],
    lazy => 1,
);

use List::Util 'shuffle';

my @boxes;
BOX: for ( 0 .. 50 ) {
    my $new = Box->new(
        size => [ 3 + int( rand 2 ), 2 + int( rand 2 ) ] );
#         size => [ 2, 2 ] );

    $new->position([
        int( rand( $tiles_wide - $new->width ) ),
        int( rand( $tiles_high - $new->height ) ),
    ]);

    for my $old (@boxes) {
        $old->position, $old->position + $old->size;
        next BOX if $new->overlaps_with($old);
    }

    push @boxes, $new;
    draw_box($term, $new);
    $term->refresh;
}

$term->set_color( WHITE, BLACK );

my $region = 0;
for my $box (@boxes) {
    $region++;
    my $position = $box->position;
    for my $x ( 0 .. $box->width-1 ) {
        for my $y ( 0 .. $box->height-1 ) {
            my $coord = $position + [ $x, $y ];

            next unless $grid->in_bounds( $coord );

            my $cell = $grid->add_cell( $coord );
            $cell->stash->{region} = $region;
        }
    }
}

$grid->connect_to_neighbours($_) for $grid->nodes;

for my $box (@boxes) {
    my $position = $box->position;
    for my $x ( -1, $box->width ) {
        for my $y ( -1, $box->height ) {
            my $coord = $position + [ $x, $y ];

            next unless $grid->in_bounds( $coord );

            my $cell = $grid->add_cell( $coord );
            $cell->stash->{region} = 0;
        }
    }
}

$grid->lock;
draw_grid($term, $grid);
$term->refresh;
<>;

use Graph::Base::Traversal::DepthFirst;
my $traversal = Graph::Base::Traversal::DepthFirst->new(
    graph => $grid->new(
        dimensions => $grid->dimensions,
        lazy => 0,
    ),
    next_root => sub {
        my ( $graph, $seen ) = @_;
        $grid->lock;

        my ( $w, $h ) = @{ $graph->dimensions };

        my $root;
        for my $node ( $graph->nodes ) {
            next if $seen->{ $node->id };

            my ( $x, $y ) = @{ $node->coord };

            next unless $x && $y;
            next if $x == $w-1 || $y == $h-1;

            my $good = 1;

            for my $coord ( $grid->moore_neighbourhood( $node->coord ) ) {
                my $neighbour = $grid->get_cell($coord) or next;

                if ( $neighbour->outgoing ) {
                    $good = 0;
                }
            }

            $root = $node and last if $good;
        }

        $grid->unlock;
        return $root;
    },
);

$grid->unlock;

use curry;
$traversal->on(
    visit => $grid->$curry::weak(sub {
        my ( $grid, $traversal, $node, $edge ) = @_;

        my $graph = $traversal->graph;

        my @edges;
        for my $direction ( shuffle @{ $grid->directions } ) {

            my ( $x, $y )   = @{ $node->coord };
            my ( $dx, $dy ) = @{ $direction };

            next if $dx && $dy;

            my $next = $grid->get_cell([ $x + $dx, $y + $dy ])
                or next;

            my @checks;

            # Moving horizontally
            if ( $dx ) {
                @checks = (
                    [ $x + $dx,     $y + $dy - 1 ],
                    [ $x + $dx,     $y + $dy     ],
                    [ $x + $dx,     $y + $dy + 1 ],
                    [ $x + $dx * 2, $y + $dy - 1 ],
                    [ $x + $dx * 2, $y + $dy     ],
                    [ $x + $dx * 2, $y + $dy + 1 ],
                );
            }
            # Moving vertically
            else {
                @checks = (
                    [ $x + $dx - 1, $y + $dy     ],
                    [ $x + $dx,     $y + $dy     ],
                    [ $x + $dx + 1, $y + $dy     ],
                    [ $x + $dx - 1, $y + $dy * 2 ],
                    [ $x + $dx,     $y + $dy * 2 ],
                    [ $x + $dx + 1, $y + $dy * 2 ],
                );
            }

            my $good = 1;
            for (@checks) {
                my $check = $grid->get_cell($_);
                if ( !$check || $check->outgoing ) {
                    $good = 0;
                    last;
                }
            }

            next unless $good;

            # Add an edge for the traversal
            $graph->add_edge( $node, $next );

            # Add an edge for the equivalent nodes in the grid
            $grid->add_edge( $node, $next );
        }
    }),
);

# $ENV{GRAPH_DEBUG} = 1;
$traversal->traverse;

draw_grid($term, $grid);
$term->refresh;

<>;

sub draw_box {
    my ( $canvas, $box ) = @_;

    my $pos = $box->position;
    my ( $w, $h ) = @{ $box->size };

    for my $x ( 0 .. $w-1 ) {
        for my $y ( 0 .. $h-1 ) {
            my $coord = $pos + [ $x, $y ];
            $canvas->char( $coord, '#' );
        }
    }
}

sub draw_grid {
    my ( $canvas, $grid ) = @_;

    my ( $w, $h ) = @{ $grid->dimensions };

    my $was_locked = $grid->locked;
    $grid->lock;
    for my $x ( 0 .. $w-1 ) {
        for my $y ( 0 .. $h-1 ) {
            my $coord = Vector->coerce([ $x, $y ]);
            my $cell = $grid->get_cell($coord);

            if ( $cell && $cell->outgoing ) {
                $canvas->char( $coord, ' ' );
            }
            else {
                $canvas->char( $coord, '#' );
            }
        }
    }
    $grid->unlock unless $was_locked;
}
